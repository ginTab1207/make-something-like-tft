﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlashingText : MonoBehaviour
{
    protected Text m_text = null;
    protected float T = 0f;
    protected float f = 0f;
    // Start is called before the first frame update
    protected virtual void Start() 
    { 
        m_text = this.GetComponent<Text>();
        T = 1.0f;
        f = 1.0f / T;
        
    }
    // Update is called once per frame
    protected virtual void Update()
    {
        float sin = Mathf.Clamp(Mathf.Sin(2 * Mathf.PI * f * Time.time),0.01f,1f);
        m_text.color = new Color(1f,1f,1f,sin);
    }
}
