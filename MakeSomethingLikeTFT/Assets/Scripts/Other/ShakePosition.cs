﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakePosition : MonoBehaviour
{
    float T = 0f;
    float f = 0f;
    // Start is called before the first frame update
    void Start()
    {
        T = 1.0f;
        f = 1.0f / T;
    }

    // Update is called once per frame
    void Update()
    {       
        float sin = Mathf.Sin(2 * Mathf.PI * f * Time.time);
        this.transform.position += new Vector3(0, sin / 150, 0);
    }
}
