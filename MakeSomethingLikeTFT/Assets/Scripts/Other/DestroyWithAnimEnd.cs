﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyWithAnimEnd : MonoBehaviour
{
    protected Animator m_anim;   
    protected virtual void Start()
    {
        m_anim = GetComponent<Animator>();
    }
    protected virtual void Update()
    {
        if (m_anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.93f)
        {
            Destroy(gameObject);
        }
    }
}
