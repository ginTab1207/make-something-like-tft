﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutIN : DestroyWithAnimEnd
{
    [SerializeField] GameObject parentObj = null;
    [SerializeField, Tooltip("スライドする時のSE")] AudioClip slideSE = null;
    [SerializeField, Tooltip("スライド終わりのSE")] AudioClip slideStopSE = null;
    private AudioSource m_audiosource = null;
    protected override void Start()
    {
        base.Start();
        m_audiosource = GetComponent<AudioSource>();        
        m_audiosource.PlayOneShot(slideSE);
    }
    protected override void Update()
    {
        if (m_anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.95f)
        {
            Destroy(parentObj);
        }
    }
    public void PlaySE() 
    {
        m_audiosource.PlayOneShot(slideStopSE);
    }
}
