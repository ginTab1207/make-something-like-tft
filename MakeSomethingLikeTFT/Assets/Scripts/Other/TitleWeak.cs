﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TitleWeak : MonoBehaviour
{
    private Animator m_animator = null;
    private SpriteRenderer m_spriteRenderer = null;
    public IEnumerator TakeAttack()
    {
        m_animator.SetTrigger("Hurt");
        yield return new WaitForSeconds(0.5f);
        m_animator.SetTrigger("Death");
        StartCoroutine(TitleImageManager.Instance.FadeSprite(this.GetComponent<SpriteRenderer>(), -0.01f, false,()=> { }));
        Instantiate(Resources.Load<ParticleSystem>("ParticleSystems/DestroyEffect"),transform.position,Quaternion.identity);
    }
    private void OnEnable()
    {
        m_animator = m_animator ?? GetComponent<Animator>();
        m_spriteRenderer = m_spriteRenderer ?? GetComponent<SpriteRenderer>();
        m_spriteRenderer.color = Color.white;
    }
}
