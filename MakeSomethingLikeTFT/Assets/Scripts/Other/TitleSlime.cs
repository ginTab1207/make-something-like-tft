﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TitleSlime : MonoBehaviour
{
    private TitleWeak titleweak = null;
    private AudioSource m_audioSource = null;
    private SpriteRenderer m_sprite = null;
    [SerializeField] AudioClip AttackSE = null;
    [SerializeField] AudioClip WalkSE = null;
    public IEnumerator TitleAttack()
    {
        titleweak?.StartCoroutine("TakeAttack");
        m_audioSource.Stop();
        m_audioSource.PlayOneShot(AttackSE);
        while (m_audioSource.isPlaying)
        {
            yield return null;
        }
        StartCoroutine(TitleImageManager.Instance.FadeSprite(m_sprite, -0.01f, false,()=>TitleManager.Instance.StateChange(TitleState.WaitClick)));
    }

    private void OnEnable()
    {
        m_audioSource = m_audioSource ?? GetComponent<AudioSource>();
        titleweak = titleweak ?? FindObjectOfType<TitleWeak>();
        m_sprite = m_sprite ?? GetComponent<SpriteRenderer>();
        m_audioSource.clip = WalkSE;
        m_audioSource.Play();
        m_sprite.color = Color.white;
    }
}
