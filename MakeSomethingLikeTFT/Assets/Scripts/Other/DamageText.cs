﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageText : DestroyWithAnimEnd
{
    [SerializeField] Text m_text = null;
    protected override void Update()
    {
        base.Update();
        gameObject.transform.localPosition += Vector3.up;
    }
    public void SetText(float damage)
    {
        m_text.text = damage.ToString();
        Vector3 pos = gameObject.transform.localPosition;
        gameObject.transform.localPosition = new Vector3(pos.x + Random.Range(-5, 5f),pos.y + Random.Range(-5, 5f), 10f); 
    }

}
