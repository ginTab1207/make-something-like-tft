﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExplainBoard : MonoBehaviour
{
    [SerializeField] Text HPText = null;
    [SerializeField] Text AttackPowerText = null;
    [SerializeField] Text SkillNameText = null;
    [SerializeField] Text ExplainSkillText = null;
    [SerializeField] Image SynergyImage = null;
    [SerializeField] Text SynergyText = null;
    [SerializeField] GameObject ExplainSkillObj = null;
    public void SetBoard(string hp, string attackPower, string skillName, string explain,Image image,string synergyText)
    {
        HPText.text = hp;
        AttackPowerText.text = attackPower;
        SkillNameText.text = skillName;
        ExplainSkillText.text = explain;
        SynergyImage.sprite = image.sprite;
        SynergyText.text = synergyText;
        ExplainSkillObj.SetActive(false);
    }
    private void Update()
    {
        RaycastHit2D Hit2D = Mouse.Instance.GetMouseRaycastHit2D();
        if (Hit2D.collider == null) return;
        if (Hit2D.transform.gameObject.name == "SkillImage")
        {
            ExplainSkillObj.SetActive(true);
        }
        else
        {
            ExplainSkillObj.SetActive(false);
        }
    }
}
