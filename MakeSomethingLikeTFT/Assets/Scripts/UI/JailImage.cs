﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JailImage : MonoBehaviour
{
    Animator m_anim = null;
    private void Start()
    {
        m_anim = gameObject.GetComponent<Animator>();
        var phaseManager = PhaseManager.Instance;
        phaseManager.OnBeginBattle += Action;
        phaseManager.OnBeginBuy += Action2;
    }
    private void Action()
    {
        m_anim.SetInteger("AnimState", 1);
    }
    private void Action2()
    {
        m_anim.SetInteger("AnimState", 2);
    }
    private void Reset()
    {
        m_anim.SetInteger("AnimState", 0);
    }
}
