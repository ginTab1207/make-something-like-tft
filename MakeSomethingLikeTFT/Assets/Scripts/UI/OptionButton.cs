﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
public class OptionButton : MonoBehaviour
{
    private Button m_button = null;

    void OnEnable()
    {
        m_button = GetComponent<Button>();
        m_button.onClick.AddListener(AudioManager.Instance.ActiveAudioPanel);
    }
}
