﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionSlider : MonoBehaviour
{
    private Slider m_slider = null;
    // Start is called before the first frame update
    void OnEnable()
    {
        m_slider = GetComponentInChildren<Slider>();
        m_slider.onValueChanged.AddListener(AudioManager.Instance.SetVolume);
        m_slider.value = AudioManager.Instance.AudioVolume;
    }
    public void DestroyPanel()
    {
        AudioManager.Instance.DestroyAudioPanel();
    }
}
