﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Screw : ItemBase
{
    public override void Use(Monster monster)
    {
        monster?.ChangeAttackPower(0.5f);
        Destroy(this.gameObject);
    }
}
