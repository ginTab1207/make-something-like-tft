﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmptyCan : ItemBase
{
    public override void Use(Monster monster)
    {
        monster?.TakeHeal(4f);
        Destroy(this.gameObject);
    }
}
