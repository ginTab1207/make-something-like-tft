﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spring : ItemBase
{
    public override void Use(Monster monster)
    {
        monster?.ChangeAttackSpeed(0.5f);
        Destroy(this.gameObject);
    }
}
