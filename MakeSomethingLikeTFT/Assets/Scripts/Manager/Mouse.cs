﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using UnityEngine.UI;

public class Mouse : Singleton<Mouse>
{
    private enum MouseState
    {
        IDLE,
        ISLEFTCLICK,
        ISRIGHTCLICK
    }
    [SerializeField] GameObject clickedObjectRoot = null;
    [SerializeField] GameObject SynergyExplainRoot = null;
    [SerializeField] Texture2D[] cursorTexture = new Texture2D[3];
    [SerializeField] AudioClip placeSE = null;
    [SerializeField] AudioSource audioSource = null;
    public GameObject ClickedObject { get; private set; } = null;
    private Monster ClickMonster = null;
    private GameObject SynergyExplain = null;
    private GameObject currentCoaster = null;
    private CursorMode cursorMode = CursorMode.ForceSoftware;
    private Vector2 hotSpot = Vector2.zero;
    private MouseState currentState = MouseState.IDLE;
    private Image objImage;
    private Color DefaultColor;
    protected override void Awake()
    {
        base.Awake();
        Cursor.SetCursor(cursorTexture[0], hotSpot, cursorMode);
    }
    void Update()
    {
        switch (currentState)
        {
            case MouseState.IDLE:
                if (Input.GetMouseButton(0)) Click();
                if (Input.GetMouseButton(1)) RightClick();
                break;
            case MouseState.ISLEFTCLICK:
                Cursor.SetCursor(cursorTexture[1], hotSpot, cursorMode);
                if (ClickedObject != null)
                {
                    Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    pos = clickedObjectRoot.transform.InverseTransformPoint(pos);
                    ClickedObject.transform.localPosition = new Vector3(pos.x, pos.y, 0f);
                    ClickedObject.GetComponent<Image>().color = new Color(1f, 1f, 1f, Mathf.PingPong(Time.time, 1f));
                }
                if (Input.GetMouseButtonUp(0))
                {
                    currentState = MouseState.IDLE;
                    
                    Cursor.SetCursor(cursorTexture[0], hotSpot, cursorMode);
                    if (ClickedObject != null)
                    {
                        var fieldManager = FieldManager.Instance;
                        var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                        audioSource.PlayOneShot(placeSE);
                        if (ClickedObject.tag == "Item")
                        {
                            Monster nearMons = FieldManager.Instance.GetNearestMonster(mousePos);
                            if (nearMons != null)
                            {
                                ClickedObject.GetComponent<ItemBase>().Use(nearMons);
                                ClickedObject = null;
                                objImage = null;
                                return;
                            }
                            else
                            {
                                ClickedObject.transform.SetParent(currentCoaster.transform);
                            }
                        }
                        else if (ClickedObject.tag == "Monster")
                        {
                            if (GetHitSELLPanel())
                            {
                                ClickMonster.SELL();
                                ClickedObject = null;
                                objImage = null;
                                Gacha.Instance.ChangeShopMode(ShopMode.BUY);
                                return;
                            }
                            var nearestCoaster = fieldManager.GetNearestEmptyCoaster(mousePos);
                            if (nearestCoaster != null) 
                            {
                                ClickedObject.transform.SetParent(nearestCoaster.transform);
                            }
                            else
                            {
                                ClickedObject.transform.SetParent(currentCoaster.transform);
                            }
                            ClickMonster = null;
                        }                       
                        ClickedObject.transform.localPosition = Vector3.zero;
                        if (objImage != null)
                        {
                            objImage.color = DefaultColor;
                            objImage = null;
                        }                        
                    }
                    Gacha.Instance.ChangeShopMode(ShopMode.BUY);
                    ClickedObject = null;                    
                }
                break;
            case MouseState.ISRIGHTCLICK:

                if (Input.GetMouseButtonUp(1))
                {
                    currentState = MouseState.IDLE;
                    Cursor.SetCursor(cursorTexture[0], hotSpot, cursorMode);
                }
                break;
            default:
                break;
        }
    }
    private void Click()
    {
        currentState = MouseState.ISLEFTCLICK;
        RaycastHit2D Hit2D = GetMouseRaycastHit2D();
        Cursor.SetCursor(cursorTexture[2], hotSpot, cursorMode);
        if (Hit2D.collider != null)
        {
            if (Hit2D.collider.gameObject.CompareTag("Monster"))
            {
                Monster mons = Hit2D.collider.gameObject.GetComponent<Monster>();
                if (mons.m_isBattle == false)
                {
                    OnClickObject(Hit2D);
                    ClickMonster = mons;
                    Gacha.Instance.ChangeShopMode(ShopMode.SELL);
                }
            }
            else if (Hit2D.collider.gameObject.CompareTag("Item"))
            {
                OnClickObject(Hit2D);
            }
        }        
    }
    private void OnClickObject(RaycastHit2D Hit2D)
    {
        ClickedObject = Hit2D.collider.gameObject;
        currentCoaster = ClickedObject.transform.parent.gameObject;
        ClickedObject.transform.SetParent(clickedObjectRoot.transform);
        if (ClickedObject.GetComponent<Image>() != null)
        {
            objImage = ClickedObject.GetComponent<Image>();
            DefaultColor = objImage.color;
        }
    }
    private void RightClick()
    {
        currentState = MouseState.ISRIGHTCLICK;
        Cursor.SetCursor(cursorTexture[2], hotSpot, cursorMode);
        RaycastHit2D Hit2D = GetMouseRaycastHit2D();
        if (SynergyExplain != null) Destroy(SynergyExplain.gameObject, 0.1f);
        if (Hit2D.collider == null)
        {
            return;
        }
        else if (Hit2D.collider.name == "UndeadIcon") SynergyExplain = Instantiate(Resources.Load<GameObject>("UIPrefabs/UndeadExplain"), SynergyExplainRoot.transform);
        else if (Hit2D.collider.name == "NatureIcon") SynergyExplain = Instantiate(Resources.Load<GameObject>("UIPrefabs/NatureExplain"), SynergyExplainRoot.transform);
        else if (Hit2D.collider.name == "FairyIcon") SynergyExplain = Instantiate(Resources.Load<GameObject>("UIPrefabs/FairyExplain"), SynergyExplainRoot.transform);
        else if (Hit2D.collider.name == "RobotIcon") SynergyExplain = Instantiate(Resources.Load<GameObject>("UIPrefabs/RobotExplain"), SynergyExplainRoot.transform);
        else if (Hit2D.collider.name == "HumanIcon") SynergyExplain = Instantiate(Resources.Load<GameObject>("UIPrefabs/HumanExplain"), SynergyExplainRoot.transform);
        else if (Hit2D.collider.gameObject.CompareTag("Monster")) ExplainBoardManager.Instance.SetExplainBoard(Hit2D.transform.gameObject);
    }
    public RaycastHit2D GetMouseRaycastHit2D()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D Hit2D = Physics2D.Raycast(ray.origin, ray.direction);
        return Hit2D;
    }
    public bool GetHitSELLPanel()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        foreach (RaycastHit2D hit in Physics2D.RaycastAll(ray.origin, ray.direction))
        {
            //オブジェクトが見つかったときの処理
            if (hit)
            {
                //タグが売却パネルなら
                if (hit.collider.gameObject.CompareTag("SELLPanel") && ClickMonster != null)
                {
                    return true;
                }
            }
        }
        return false;
    }
}

