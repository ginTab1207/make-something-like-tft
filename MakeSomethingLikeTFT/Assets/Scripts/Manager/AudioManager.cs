﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>OptionManager</summary>
public class AudioManager : Singleton<AudioManager>
{
    private GameObject canvas = null;
    public AudioSource[] audioSources { get; private set; } = null;
    private GameObject optionPanel = null;
    private GameObject explainPanel = null;
    public float AudioVolume { get; private set; } = 0.5f;
    private void Start()
    {
        StartTitleSetting();
    }
    private void Update()
    {
        if (LocalSceneManager.Instance.CurrentScene != "Title") return;
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (optionPanel == null)
            {
                ActiveAudioPanel();
            }
            else
            {
                DestroyAudioPanel();
            }
        }
    }
    //public void ActiveOptionButton()
    //{
    //    if (optionButton == null)
    //    {
    //        TitleButtonRoot = GameObject.Find("TitleButtonRoot");
    //        optionButton = Instantiate(Resources.Load<GameObject>("UIPrefabs/OptionButton"), TitleButtonRoot.transform);
    //    }
    //    else
    //    {
    //        Debug.LogWarning("すでにOptionButtonは生成されています");
    //    }
    //}
    //public void DestroyOptionButton()
    //{
    //    if (optionButton != null)
    //    {
    //        Destroy(optionButton);
    //        optionButton = null;
    //    }
    //    else
    //    {
    //        Debug.LogWarning("すでにOptionButtonは破棄されています");
    //    }
    //}
    public void ActiveAudioPanel()
    {
        if (optionPanel == null)
        {
            optionPanel = Instantiate(Resources.Load<GameObject>("UIPrefabs/Option"), canvas.transform);
        }
        else
        {
            Debug.LogWarning("すでにOptionPanelは生成されています");
        }
    }
    public void DestroyAudioPanel()
    {
        if (optionPanel != null)
        {
            Destroy(optionPanel);
            optionPanel = null;
        }
        else
        {
            Debug.LogWarning("すでにOptionPanelは破棄されています");
        }
    }
    public void ActiveExplainPanel()
    {
        if (explainPanel == null)
        {
            explainPanel = Instantiate(Resources.Load<GameObject>("UIPrefabs/TitleExplain"), canvas.transform);
        }
        else
        {
            Debug.LogWarning("すでにExplainPanelは生成されています");
        }
    }
    public void DestroyExplainPnael()
    {
        if (explainPanel != null)
        {
            Destroy(explainPanel);
        }
        else
        {
            Debug.LogWarning("すでにExplainPanelは破棄されています");
        }
    }
    public void SetVolume(float value)
    {
        AudioVolume = value;
        foreach (var audioSource in audioSources)
        {
            audioSource.volume = AudioVolume;
        }
    }
    public void SetAudioSources()
    {
        audioSources = FindObjectsOfType<AudioSource>();
        foreach (var audioSource in audioSources)
        {
            audioSource.volume = AudioVolume;
            Debug.Log(audioSource.gameObject);
        }
        //SetVolume(AudioVolume);
    }
    public void OnEndTitle()
    {
        DontDestroyOnLoad(gameObject);
    }
    public void StartTitleSetting()
    {
        SetAudioSources();
        if (canvas == null) canvas = Instantiate(Resources.Load<GameObject>("UIPrefabs/OptionCanvas"), Vector3.zero, Quaternion.identity);
        else canvas = GameObject.Find("OptionCanvas");

    }
}
