﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageTextManager : Singleton<DamageTextManager>
{
    [SerializeField] GameObject DamageTextRoot = null;
    public void InstanceDamageText(float damage,Vector3 pos)
    {
        GameObject damageText = Instantiate(Resources.Load<GameObject>("UIPrefabs/DamageText"),DamageTextRoot.transform);
        damageText.transform.position = pos;
        damageText.GetComponent<DamageText>().SetText(damage);
    }
}
