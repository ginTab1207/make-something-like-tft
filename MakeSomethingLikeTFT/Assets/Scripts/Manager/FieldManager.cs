﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class FieldManager : Singleton<FieldManager>
{
    [SerializeField] GameObject fieldCoastersRoot = null;
    public List<GameObject> FieldCoasterList { private set; get; } = new List<GameObject>();
    public List<GameObject> MyCoasterList { private set; get; } = new List<GameObject>();
    public List<GameObject> AllCoasterList { private set; get; } = new List<GameObject>();
    private void Start()
    {
        FieldCoasterList.AddRange(GameObject.FindGameObjectsWithTag("FieldCoaster"));
        MyCoasterList.AddRange(GameObject.FindGameObjectsWithTag("MyCoaster"));
        AllCoasterList = FieldCoasterList.Concat(MyCoasterList).ToList();
        fieldCoastersRoot = GameObject.Find("FieldCoasters");
    }
    public GameObject GetCheckBlankAllFieldCoaster()
    {
        foreach (var fieldCoaster in FieldCoasterList)
        {
            if (fieldCoaster.transform.childCount == 0)
            {
                return fieldCoaster;
            }
        }
        return null;
    }
    public GameObject GetBlankAllMyCoaster()
    {
        foreach (var myCoaster in MyCoasterList)
        {
            if (myCoaster.transform.childCount == 0)
            {
                return myCoaster;
            }
        }
        return null;
    }
    public GameObject GetBlankAllCoaster()
    {
        foreach (var coaster in AllCoasterList)
        {
            if (coaster.transform.childCount == 0)
            {
                return coaster;
            }
        }
        return null;
    }
    public GameObject GetNearestEmptyCoaster(Vector3 mousePos)
    {
        var nearestCoaster = GetNearestCoaster(mousePos);
        if (nearestCoaster.transform.childCount == 0)
        {
            return nearestCoaster;
        }
        else
        {
            Debug.Log("近くのコースターは空いてませんでした");
            return null;
        }       
    }
    public Monster GetNearestMonster(Vector3 mousePos)
    {
        var nearestCoaster = GetNearestCoaster(mousePos);
        if (nearestCoaster.transform.childCount != 0)
        {
            return nearestCoaster.GetComponentInChildren<Monster>();
        }
        else return null;
    }
    private GameObject GetNearestCoaster(Vector3 mousePos)
    {
        float minDistance = float.MaxValue;
        GameObject nearestCoaster = null;
        List<GameObject> CoasterList;
        
        if (PhaseManager.Instance.currentPhase == PHASE.BUY) CoasterList = AllCoasterList;
        else CoasterList = MyCoasterList;

        foreach (var coaster in CoasterList)
        {
            var distance = Vector3.Distance(mousePos, coaster.transform.position);
            if (distance < minDistance)
            {
                minDistance = distance;
                nearestCoaster = coaster;
            }
        }
        return nearestCoaster;
    }
    public void AddCoasters(int num)
    {
        for (int i = 0; i < num; i++)
        {
            GameObject clone = Instantiate(Resources.Load<GameObject>("UIPrefabs/FieldCoaster"), fieldCoastersRoot.transform);
            FieldCoasterList.Add(clone);
            AllCoasterList.Add(clone);
        }
    }
}
