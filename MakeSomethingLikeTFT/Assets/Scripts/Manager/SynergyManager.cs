﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
public enum SynergyID
{
    Undead,
    Nature,
    Fairy,
    Robot,
    Human,
    None
}
public enum SynergyEffectID
{
    Undead1,
    Undead2,
    Undead3,
    Nature1,
    Nature2,
    Nature3,
    Fairy1,
    Fairy2,
    Robot1,
    Robot2,
    Human1,
    None
}

public class SynergyManager : Singleton<SynergyManager>
{
    private static int synergyLength = System.Enum.GetValues(typeof(SynergyID)).Length;
    /// <summary>現在Activeなシナジー</summary>
    public SynergyEffectID[] ActiveSynergy { get; private set; } = new SynergyEffectID[synergyLength];
    public List<SynergyData> m_synergyDataBase { get; private set; } = new List<SynergyData>();
    public List<List<Monster>> SynergyMonster { get; private set; } = new List<List<Monster>>();
    public List<List<Text>> SynergyTexts { get; private set; } = new List<List<Text>>();
    [SerializeField] List<Text> UndeadTexts = new List<Text>();
    [SerializeField] List<Text> NatureTexts = new List<Text>();
    [SerializeField] List<Text> FairyTexts = new List<Text>();
    [SerializeField] List<Text> RobotTexts = new List<Text>();
    [SerializeField] List<Text> HumanTexts = new List<Text>();
    [SerializeField] public List<Image> SynergyImages = new List<Image>();
    List<Text> ActiveTexts = null;
    int[] synergyPower = new int[synergyLength];
    [SerializeField] private Color activeTextColor = default;
    [SerializeField] private Color deActiveTextColor = default;
    private bool stopLoop = false;   
    private void Start()
    {
        ActiveTexts = new List<Text>();
        for (int i = 0; i < ActiveSynergy.Length; i++)
        {
            ActiveSynergy[i] = SynergyEffectID.None;
            SynergyMonster.Add(new List<Monster>());
        }
        m_synergyDataBase = Resources.Load<SynergyDataBase>("ScriptableObject/SynergyDataBase").SynergyDataList;
        SynergyTexts.Add(UndeadTexts);
        SynergyTexts.Add(NatureTexts);
        SynergyTexts.Add(FairyTexts);
        SynergyTexts.Add(RobotTexts);
        SynergyTexts.Add(HumanTexts);
        foreach (var texts in SynergyTexts)
        {
            foreach (var text in texts)
            {
                text.color = deActiveTextColor;
            }
        }
    }
    public void ActivateSynergies()
    {
        var fightingMonster = BattleManager.Instance.fightingMonster;
        SynergyMonster.Clear();
        for (int i = 0; i < ActiveSynergy.Length; i++)
        {
            ActiveSynergy[i] = SynergyEffectID.None;
            SynergyMonster.Add(new List<Monster>());
        }
        for (int i = 0; i < synergyPower.Length; i++) synergyPower[i] = 0;
        bool[] checkSame = new bool[(int)MonstersID.Durahan + 1];
        foreach (var monster in fightingMonster)
        {
            SynergyID syn = monster.m_MonsterData.Synergy;
            for (int i = 0; i < checkSame.Length; i++)
            {
                if (i == (int)monster.m_MonsterData.MonsterName || i == ((int)monster.m_MonsterData.MonsterName - ((int)MonstersID.EvoSlime)))
                {
                    SynergyMonster[(int)syn].Add(monster);
                    if (checkSame[i] == true) break;//同じモンスター(進化後含め)が被ってた場合シナジーパワーは上がらない
                    checkSame[i] = true;
                    synergyPower[(int)syn]++;
                    break;
                }
            }
        }
        for (int synergyID = 0; synergyID < synergyLength; synergyID++)
        {
            for (int i = m_synergyDataBase[synergyID].RequiredMonster.Length - 1; i >= 0; i--)
            {
                if (synergyPower[synergyID] >= m_synergyDataBase[synergyID].RequiredMonster[i])
                {
                    ActiveSynergy[synergyID] = m_synergyDataBase[synergyID].SynergyEffectIDs[i];                    
                    ActiveTexts.Add(SynergyTexts[synergyID][i]);
                    break;
                }
            }
        }
        foreach (var text in ActiveTexts)
        {
            text.color = activeTextColor;
        }
        foreach (var syn in ActiveSynergy)
        {
            ActiveEffect(syn);
        }
    }

    /// <summary>シナジーの効果付与</summary>
    /// <param name="synergyEffect"></param>
    public void ActiveEffect(SynergyEffectID synergyEffect)
    {
        switch (synergyEffect)
        {
            case SynergyEffectID.Undead1:
                BattleManager.Instance.fightingEnemies.ForEach(x => x.ChangeAttackPower(-0.5f));
                break;
            case SynergyEffectID.Undead2:
                BattleManager.Instance.fightingEnemies.ForEach(x => x.ChangeAttackPower(-1f));
                break;
            case SynergyEffectID.Undead3:
                BattleManager.Instance.fightingEnemies.ForEach(x => x.ChangeAttackPower(-2f));
                break;
            case SynergyEffectID.Nature1:
                SynergyMonster[(int)SynergyID.Nature].ForEach(x => x.ChangeEvansiveness(10f));
                break;
            case SynergyEffectID.Nature2:
                SynergyMonster[(int)SynergyID.Nature].ForEach(x => x.ChangeEvansiveness(20f));
                break;
            case SynergyEffectID.Nature3:
                SynergyMonster[(int)SynergyID.Nature].ForEach(x => x.ChangeEvansiveness(40f));
                break;
            case SynergyEffectID.Fairy1:
            case SynergyEffectID.Fairy2:
                stopLoop = false;
                StartCoroutine(LoopSynergyEffect(SynergyID.Fairy, synergyEffect, 3f));
                break;
            case SynergyEffectID.Robot1:
            case SynergyEffectID.Robot2:
                break;
            case SynergyEffectID.Human1:
                SynergyMonster.ForEach(x => x.ForEach(y => y.ChangeAttackSpeed(0.5f)));
                break;
            case SynergyEffectID.None:
                return;
        }
    }
    /// <summary>継続的にかけるシナジー効果</summary>
    /// <param name="synergyEffect"></param>
    private IEnumerator LoopSynergyEffect(SynergyID synergyID, SynergyEffectID synergyEffect,float LoopTime)
    {
        while (!stopLoop)
        {
            switch (synergyEffect)
            {
                case SynergyEffectID.Fairy1:
                    if (SynergyMonster[(int)synergyID] != null)
                    {
                        SynergyMonster[(int)synergyID].ForEach(x => x.TakeHeal(Random.Range(1, 3)));
                    }
                    break;
                case SynergyEffectID.Fairy2:
                    if (SynergyMonster[(int)synergyID] != null)
                    {
                        SynergyMonster[(int)synergyID].ForEach(x => x.TakeHeal(Random.Range(2, 4)));
                    }
                    break;
            }
            yield return new WaitForSeconds(LoopTime);
        }
        yield break;
    }
    /// <summary>シナジー効果解除</summary>
    public void DeActiveEffect()
    {
        foreach (SynergyEffectID synergyEffectID in ActiveSynergy)
        {
            switch (synergyEffectID)
            {
                case SynergyEffectID.Undead1:
                    BattleManager.Instance.fightingEnemies.ForEach(x => x.ChangeAttackPower(0.5f));
                    break;
                case SynergyEffectID.Undead2:
                    BattleManager.Instance.fightingEnemies.ForEach(x => x.ChangeAttackPower(1f));
                    break;
                case SynergyEffectID.Undead3:
                    BattleManager.Instance.fightingEnemies.ForEach(x => x.ChangeAttackPower(2f));
                    break;
                case SynergyEffectID.Nature1:
                    if (SynergyMonster[(int)SynergyID.Nature] != null)
                    {
                        SynergyMonster[(int)SynergyID.Nature].ForEach(x => x.ChangeEvansiveness(-10f));
                    }
                    break;
                case SynergyEffectID.Nature2:
                    if (SynergyMonster[(int)SynergyID.Nature] != null)
                    {
                        SynergyMonster[(int)SynergyID.Nature].ForEach(x => x.ChangeEvansiveness(-20f));
                    }
                    break;
                case SynergyEffectID.Nature3:
                    if (SynergyMonster[(int)SynergyID.Nature] != null)
                    {
                        SynergyMonster[(int)SynergyID.Nature].ForEach(x => x.ChangeEvansiveness(-40f));
                    }
                    break;
                case SynergyEffectID.Fairy1:
                case SynergyEffectID.Fairy2:
                    stopLoop = true;
                    break;
                case SynergyEffectID.Robot1:
                case SynergyEffectID.Robot2:
                    GiveRobotItem(synergyEffectID);
                    break;
                case SynergyEffectID.Human1:
                    if (SynergyMonster[(int)SynergyID.Human] != null)
                    {
                        SynergyMonster[(int)SynergyID.Human].ForEach(x => x.ChangeAttackSpeed(-0.5f));
                    }
                    break;
                case SynergyEffectID.None:
                    break;
            }
        }
        for (int synergyID = 0; synergyID < ActiveTexts.Count; synergyID++)
        {
            ActiveTexts[synergyID].color = deActiveTextColor;
        }
        ActiveTexts.Clear();
        ActiveTexts = new List<Text>();
        
    }
    public void GiveRobotItem(SynergyEffectID RobotEffectID)
    {
        int loopLength = 0;
        switch (RobotEffectID)
        {
            case SynergyEffectID.Robot1:
                loopLength = 1;
                break;
            case SynergyEffectID.Robot2:
                loopLength = 2;
                break;
            case SynergyEffectID.None:
            default:
                break;
        }
        for (int i = 0; i < loopLength; i++)
        {
            ItemID randomID = (ItemID)Random.Range(0,(int)ItemID.None - 1);
            ItemManager.Instance.GenerateItem(randomID,1);
        }
    }
}

