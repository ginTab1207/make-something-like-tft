﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanceMonsterManager : Singleton<InstanceMonsterManager>
{
    public List<GameObject> instanceMonsterObjs { private set; get; } = new List<GameObject>();
    private const int requiredEvolveUnits = 3;
    private string[] loadResouceKey = { "ScriptableObject/MonsterDataBase", "ScriptableObject/EvolveMonsterDataBase" };
    private const float evoTimeOffSet = 0.02f;
    public void SetInstanceMonsterData(GameObject instanceMonster, MonstersID monstersNum, bool isEvoMons)
    {
        instanceMonsterObjs.Add(instanceMonster);
        if (isEvoMons) return;
        if (!isEvoMons)
        {
            CheckEvolve(monstersNum);
        }
    }

    public void CheckEvolve(MonstersID monstersNum)
    {
        var monsterDataBase = Resources.Load<MonsterDataBase>(loadResouceKey[0]);
        var m_monsterName = monsterDataBase.MonsterStatusList[(int)monstersNum].MonsterName;
        List<GameObject> sameMonsters = new List<GameObject>();
        sameMonsters = instanceMonsterObjs.FindAll(x => x.GetComponent<Monster>().m_MonsterData.MonsterName == m_monsterName);
        if (sameMonsters.Count >= requiredEvolveUnits)
        {       
            foreach (var Monster in sameMonsters)
            {
                Monster.GetComponent<Monster>().Dead();
            }
            StartCoroutine(StartEvolve(Monster.DeadTime, monstersNum));
        }
    }
    public void Evolve(MonstersID monstersNum)
    {
        GameObject summonCoaster;
        var fieldManager = FieldManager.Instance;
        summonCoaster = fieldManager.GetBlankAllCoaster();
        var evoMonsterDataBase = Resources.Load<MonsterDataBase>(loadResouceKey[1]);
        var instanceMonster = Instantiate(evoMonsterDataBase.MonsterStatusList[(int)monstersNum].SelfPrefab, summonCoaster.transform);
        instanceMonster.GetComponent<Monster>().SetMonsterData(evoMonsterDataBase.MonsterStatusList[(int)monstersNum]);
        instanceMonster.transform.localPosition = new Vector3(0, 0, 0);
        SetInstanceMonsterData(instanceMonster, monstersNum, true);
    }
    public List<GameObject> GetBattleMonsters()
    {
        List<GameObject> BattleMonsters = new List<GameObject>();
        foreach (var instanceMonster in instanceMonsterObjs)
        {
            if (instanceMonster.transform.parent.tag == "FieldCoaster")
            {
                BattleMonsters.Add(instanceMonster);
            }
        }
        return BattleMonsters;
    }
    public void RemoveFromList(GameObject gameObj) 
    {
        instanceMonsterObjs.Remove(gameObj);
    }
    public void AllDeleteMonster() 
    {
        List<GameObject> copyList = new List<GameObject>(instanceMonsterObjs);
        foreach (var monster in copyList)
        {
            instanceMonsterObjs.Find(x => x == monster).GetComponent<Monster>().Dead();
        }
        instanceMonsterObjs.Clear();
    }
    /// <summary>モンスターが死んでから進化しないとコースターが満パンの時進化出来ないため</summary>
    /// <param name="deadTime"></param>
    /// <returns></returns>
    public IEnumerator StartEvolve(float deadTime, MonstersID monstersNum)
    {
        yield return new WaitForSeconds(deadTime + evoTimeOffSet);
        Evolve(monstersNum);
    }
}
