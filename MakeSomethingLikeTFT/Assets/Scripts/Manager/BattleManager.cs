﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum StatusEffect
{
    Adhesive,
    Horror,
    Stone,
    SlowStarter,
    Angry,   
}
public enum BattleSE
{
    Damage,
    Heal,
    None
}
/// <summary>
/// 戦闘を管理
/// </summary>
public class BattleManager : Singleton<BattleManager>
{
    public AudioClip damageSE = null;
    public AudioClip healSE = null;
    public const float EnemyAttackTime = 3f;
    public const float MonsterAttackTime = 3f;
    public List<Monster> fightingMonster = new List<Monster>();
    public List<Enemy> fightingEnemies = new List<Enemy>();
    public bool isUpdate { get; private set; } = false;
    private void Start()
    {
        PhaseManager.Instance.OnBeginBattle += OnBeginBattleSetting;
        PhaseManager.Instance.OnEndBattle += OnEndBattleSetting;
    }
    private void Update()
    {
        if (!isUpdate) return;
        if (fightingMonster.Count == 0)
        {
            PhaseManager.Instance.OnEndPhase(PHASE.BUY);
        }
        else if (fightingEnemies.Count == 0)
        {

            ClearStage(StageManager.Instance.CurrentStageData.NextStage);
        }
    }
    public void SetUpdate(bool b)
    {
        isUpdate = b;
    }
    public List<Monster> GetRandomFightingMonster(int NumOfAttacks)
    {
        if (fightingMonster.Count == 0)
        {
            Debug.LogWarning("モンスターがいません");
            return null;
        }
        List<Monster> takeAttackMonsters = new List<Monster>();
        while (takeAttackMonsters.Count != NumOfAttacks)
        {
            takeAttackMonsters.Add(fightingMonster[Random.Range(0, fightingMonster.Count - 1)]);
        }
        return takeAttackMonsters;
    }
    public List<Enemy> GetRandomFightingEnemies(int NumOfAttacks)
    {
        if (fightingEnemies.Count == 0)
        {
            Debug.LogWarning("敵がいません");
            return null;
        }
        List<Enemy> randomEnemies = new List<Enemy>();
        while (randomEnemies.Count != NumOfAttacks)
        {
            randomEnemies.Add(fightingEnemies[Random.Range(0, fightingEnemies.Count - 1)]);
        }
        return randomEnemies;
    }
    public Enemy GetNearestFightingEnemy(Vector3 objPos)
    {
        if (fightingEnemies.Count == 0)
        {
            Debug.LogWarning("敵がいません");
            return null;
        }
        float nearDis = float.MaxValue;
        Enemy nearEnemy = null;
        foreach (var enemy in fightingEnemies)
        {
            float tmpDis = Vector3.Distance(enemy.transform.position, objPos);
            if (nearDis > tmpDis)
            {
                nearDis = tmpDis;
                nearEnemy = enemy;
            }
        }
        return nearEnemy;
    }
    public Monster GetNearestFightingMonsters(Vector3 objPos)
    {
        if (fightingMonster.Count == 0)
        {
            Debug.LogWarning("モンスターがいません");
            return null;
        }
        float nearDis = float.MaxValue;
        Monster nearMonster = null;
        foreach (var monster in fightingMonster)
        {
            float tmpDis = Vector3.Distance(monster.transform.position, objPos);
            if (nearDis > tmpDis)
            {
                nearDis = tmpDis;
                nearMonster = monster;
            }
        }
        return nearMonster;
    }

    public void AddListMons(Monster mons)
    {
        if (!fightingMonster.Find(x => x == mons))
        {
            fightingMonster.Add(mons);
        }
        else
        {
            Debug.LogWarning("リストに追加できませんでした");
        }
    }
    public void AddListEnemy(Enemy enemy)
    {
        if (!fightingEnemies.Find(x => x == enemy))
        {
            fightingEnemies.Add(enemy);
        }
        else
        {
            Debug.LogWarning("リストに追加出来ませんでした");
        }
    }
    public void RemoveListMons(Monster mons)
    {
        if (fightingMonster.Find(x => x == mons))
        {
            fightingMonster.Remove(mons);
        }
        else
        {
            Debug.LogWarning("リストから削除出来ませんでした");
        }
    }
    public void RemoveListEnemy(Enemy enemy)
    {
        if (fightingEnemies.Find(x => x == enemy))
        {
            fightingEnemies.Remove(enemy);
        }
        else
        {
            Debug.LogWarning("リストから削除出来ませんでした");
        }
    }

    private void OnBeginBattleSetting()
    {
        SetUpdate(true);
    }
    private void OnEndBattleSetting()
    {
        fightingMonster.Clear();
        SetUpdate(false);
    }
    public void ClearStage(StageID nextStageID)
    {
        fightingMonster.ForEach(x => x.SetIsBattle(false));
        InstanceEnemyManager.Instance.ResetEnemyObjs();
        SynergyManager.Instance.DeActiveEffect();
        var stageManager = StageManager.Instance;
        if (stageManager.CurrentStageID == StageID.Tutorial) TutorialManager.Instance.OnEndTutorialStage();
        Gacha.Instance.GetStageClearGold(stageManager.CurrentStageData.ClearGold);
        StartCoroutine(stageManager.WaitForNext(nextStageID));
        SetUpdate(false);
    }
}
