﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
/// <summary>
/// シーンマネージャーの内よく使うもの
/// </summary>
public class LocalSceneManager : Singleton<LocalSceneManager>
{
    /// <summary>
    /// フェイドマネージャーを保持
    /// </summary>
    public FadeManager fadeManager { get => FadeManager.Instance; set => _ = value; }
    /// <summary>
    /// 現在のシーン名
    /// </summary>
    public string CurrentScene = string.Empty;
    /// <summary>
    /// シーン切り替え中かどうか
    /// </summary>
    public bool isChanging { get; private set; } = false;
    protected override void Awake()
    {
        CurrentScene = SceneManager.GetActiveScene().name;
        Screen.SetResolution(1024,768,false,60);
        base.Awake();
        DontDestroyOnLoad(gameObject);
    }
    /// <summary>
    /// シーンを切り替える
    /// </summary>
    /// <param name="scene">次のシーン名</param>
    /// <param name="fadeTime">フェイドエフェクトに掛ける時間</param>
    public void SceneChange(string scene,float fadeTime) 
    {
        isChanging = true;
        fadeManager.LoadScene(scene, fadeTime);
        StartCoroutine(OnEndLoadScene(scene));
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="fadeTime"></param>
    /// <param name="stageID"></param>
    public void SceneChangeToGame(float fadeTime,StageID stageID)
    {
        isChanging = true;
        fadeManager.LoadScene("Game", fadeTime);
        StartCoroutine(OnEndLoadScene("Game", stageID));
    }
    IEnumerator OnEndLoadScene(string scene)
    {
        while (scene != SceneManager.GetActiveScene().name) yield return null;
        AudioManager.Instance.SetAudioSources();
        if (scene == "Title")
        {
            AudioManager.Instance.StartTitleSetting();
        }
        CurrentScene = scene;
        isChanging = false;
    }
    IEnumerator OnEndLoadScene(string scene,StageID stageID)
    {        
        while (scene != SceneManager.GetActiveScene().name) yield return null;
        StageManager.Instance.SetStageData(stageID);
        if (scene == "Game" && stageID == StageID.Tutorial)
        {
            TutorialManager.Instance.StartTutorial();
            AudioManager.Instance.SetAudioSources();
        }
        CurrentScene = scene;
        isChanging = false;
    }
}
