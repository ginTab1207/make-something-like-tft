﻿using UnityEngine;
using UnityEngine.UI;
enum PlayerLevel
{
    Lv1,
    Lv2,
    LV3,
}
public enum ShopMode
{
    BUY,
    SELL,
}
//ショップ管理
public class Gacha : Singleton<Gacha>
{
    ///<summary>購入失敗時に表示するテキストの親オブジェクト</summary>
    [SerializeField] GameObject cantBuyTextRoot = null;
    ///<summary>ショップが更新された際のサウンド</summary>
    [SerializeField] AudioClip ReRollSE = null;
    ///<summary>経験値購入時のサウンド</summary>
    [SerializeField] AudioClip BuyXPSE = null;
    ///<summary>レベルアップ時のサウンド</summary>
    [SerializeField] AudioClip LvUPSE = null;
    ///<summary>モンスターをドラッグ&ドロップで売却出来るパネル</summary>
    [SerializeField] GameObject sellPanel = null;
    ///<summary>購入失敗時のサウンド</summary>
    public AudioClip CantBuySE = null;
    ///<summary>購入時のサウンド</summary>
    public AudioClip BuySE = null;
    /// <summary>プレイヤーゴールド表示用テキスト </summary>
    public Text PlayerGoldText = null;
    /// <summary>プレイヤーレベル表示用テキスト </summary>
    public Text PlayerLevelText = null;
    /// <summary>売り切れ時画像</summary>
    public Sprite soldOut = null;
    /// <summary>ショップ欄の画像を保持しておくイメージ</summary>
    public Image[] GachaImage = new Image[3];
    /// <summary>ショップに並んでいる各モンスターの値段</summary>
    private int[] GachaPrice = new int[3];
    /// <summary>プレイヤーゴールドの現在値</summary>
    public int PlayerGold { get; private set; }
    /// <summary>プレイヤー経験値の現在値</summary>
    public int PlayerXP { get; private set; }
    /// <summary>次のレベルに上がる為に必要な経験値</summary>
    public int[] requiredLvUpXP { get; private set; } = { 10, 20, 0 };//ここ変数から代入した方がいい
    /// <summary>ランダムな値を保持しておく変数</summary>
    private float RandomValue = 0f;
    /// <summary>ショップを更新する際に必要なゴールド</summary>
    public const int ReRollCost = 2;
    /// <summary>経験値購入時に必要なゴールド</summary>
    public const int XPCost = 2;
    /// <summary>購入時に貰える経験値量</summary>
    public const int XP = 2;
    /// <summary>レベルアップ時にモンスタースロットをいくつ追加するか</summary>
    public const int AddCoaster = 3;
    /// <summary>ガチャ関係のSE鳴らす用のオーディオ</summary>
    private AudioSource audioSource = null;
    /// <summary>現在のプレイヤーレベル</summary>
    PlayerLevel playerLevel;
    /// <summary>ショップが購入出来る状態かを管理する</summary>
    private ShopMode currentShopMode = ShopMode.BUY;
    /// <summary>
    /// プレイヤーレベルによってのリロール時のコスト毎の確率
    /// </summary>
    private int[,] rerollProbability = new int[(int)PlayerLevel.LV3,(int)Cost.COST_FIVE];
    /// <summary>
    /// プレイヤーレベルによってのリロール時のコスト毎の確率の呼び出し時
    /// </summary>
    /// <param name="level">プレイヤーレベル</param>
    /// <param name="cost">コスト</param>
    /// <returns>確率</returns>
    public int RerollProbability(int level,int cost) => rerollProbability[level,cost];
    /// <summary>購入失敗時のテキストをロードするキー</summary>
    public enum LoadKeys
    {
        BattlePhase,
        SoldOut,
        BlankCoaster,
        NoGold,
        MaxLv,
        None
    }
    /// <summary>モンスターのコスト一覧</summary>
    public enum Cost
    {
        None,
        COST_ONE,
        COST_TWO,
        COST_THREE,
        COST_FOUR,
        COST_FIVE
    }
    /// <summary></summary>
    private string[] loadKey = { "UIPrefabs/CantBuyBattlePhase", "UIPrefabs/CantBuySoldOut", "UIPrefabs/CantBuyBlankCoaster", "UIPrefabs/CantBuyNoGold", "UIPrefabs/CantBuyMaxLv" };
    public delegate void BuyCallBack();
    public BuyCallBack buyCallBack;
    void Start()
    {
        Reset();
        audioSource = GetComponent<AudioSource>();
    }
    /// <summary>ショップ及びプレイヤーを初期に戻す</summary>
    public void Reset()
    {
        PlayerGold = 10;
        playerLevel = PlayerLevel.Lv1;  
        SetXPText();
        PhaseManager.Instance.OnEndBuy += GetPhaseEndGold;
        PlayerGoldText.text = PlayerGold.ToString();
        sellPanel.SetActive(false);
    }
    private void Update()
    {
        switch (currentShopMode)
        {
            case ShopMode.BUY:
                if (Input.GetKeyDown(KeyCode.R) && PlayerGold >= 2)
                {
                    ReRoll();
                }
                if (Input.GetKeyDown(KeyCode.F))
                {
                    BuyXP();
                }
                break;
            case ShopMode.SELL:
                return;
        }
    }
    /// <summary>ショップのモードを切り替える</summary>
    /// <param name="mode">変更先のモード</param>
    public void ChangeShopMode(ShopMode mode)
    {
        currentShopMode = mode;
        switch (mode)
        {
            case ShopMode.BUY:
                sellPanel.SetActive(false);
                break;
            case ShopMode.SELL:
                sellPanel.SetActive(true);
                break;
            default:
                break;
        }
    }
    /// <summary>ショップ更新発生時</summary>
    public void ReRoll()
    {
        if (PhaseManager.Instance.currentPhase == PHASE.BATTLE)
        {
            SpawnCantBuyText(LoadKeys.BattlePhase);
            return;
        }
        if (PlayerGold >= ReRollCost)
        {            
            Roulette();
            audioSource.PlayOneShot(ReRollSE);
        }
        SetPlayerGold(-ReRollCost);
    }
    /// <summary>経験値購入時</summary>
    public void BuyXP()
    {

        if (playerLevel == PlayerLevel.LV3)
        {
            SpawnCantBuyText(LoadKeys.MaxLv);
            return;
        }
        if (PlayerGold >= XPCost)
        {
            SetPlayerXP(XP);
            SetPlayerGold(-XPCost);
            audioSource.PlayOneShot(BuyXPSE);
        }
        else
        {
            SpawnCantBuyText(LoadKeys.NoGold);
        }
    }
    /// <summary>プレイヤーの経験値を設定</summary>
    /// <param name="XP">追加する経験値</param>
    public void SetPlayerXP(int XP)
    {
        PlayerXP = Mathf.Clamp(PlayerXP + XP, 0, int.MaxValue);
        if (PlayerXP >= requiredLvUpXP[(int)playerLevel])
        {
            int overXP = PlayerXP - requiredLvUpXP[(int)playerLevel];
            Levelup();
            PlayerXP = overXP;
        }
        SetXPText();
    }
    /// <summary>プレイヤーゴールドを設定</summary>
    /// <param name="Price">変動ゴールド量</param>
    public void SetPlayerGold(int Price)
    {
        if (PlayerGold + Price < 0)
        {
            SpawnCantBuyText(LoadKeys.NoGold); //これショップ購入時に移すべき
        }
        else
        {
            PlayerGold = Mathf.Clamp(PlayerGold + Price, 0, 100);
            PlayerGoldText.text = PlayerGold.ToString();
        }
    }
    /// <summary>必要経験値表示テキストを設定 </summary>
    public void SetXPText()
    {
        PlayerLevelText.text = playerLevel + "\n次のレベルまで " + (requiredLvUpXP[(int)playerLevel] - PlayerXP)+ " XP";
    }
    /// <summary>ショップ更新時の表示するモンスターのコストを設定</summary>
    public void Roulette()
    {
        for (int i = 0; i < 3; i++)
        {
            RandomValue = RandomPercent();
            switch (playerLevel)
            {
                case PlayerLevel.Lv1:
                    if (RandomValue <= 60)
                    {
                        SetRouletteResult(Cost.COST_ONE, i);
                    }
                    else if (RandomValue <= 80)
                    {
                        SetRouletteResult(Cost.COST_TWO, i);
                    }
                    else if (RandomValue <= 95)
                    {
                        SetRouletteResult(Cost.COST_THREE, i);
                    }
                    else
                    {
                        SetRouletteResult(Cost.COST_FOUR, i);
                    }
                    break;
                    
                ///////////////////////////////////////////////////////////////////
                case PlayerLevel.Lv2:
                    if (RandomValue <= RerollProbability(1, 1))
                    {
                        SetRouletteResult(Cost.COST_ONE, i);
                    }
                    else if (RandomValue <= 40)
                    {
                        SetRouletteResult(Cost.COST_TWO, i);
                    }
                    else if (RandomValue <= 75)
                    {
                        SetRouletteResult(Cost.COST_THREE, i);
                    }
                    else if (RandomValue <= 90)
                    {
                        SetRouletteResult(Cost.COST_FOUR, i);
                    }
                    else
                    {
                        SetRouletteResult(Cost.COST_FIVE, i);
                    }
                    break;
                ///////////////////////////////////////////////////////
                case PlayerLevel.LV3:
                    if (RandomValue <= 15)
                    {
                        SetRouletteResult(Cost.COST_ONE, i);
                    }
                    else if (RandomValue <= 30)
                    {
                        SetRouletteResult(Cost.COST_TWO, i);
                    }
                    else if (RandomValue <= 45)
                    {
                        SetRouletteResult(Cost.COST_THREE, i);
                    }
                    else if (RandomValue <= 80)
                    {
                        SetRouletteResult(Cost.COST_FOUR, i);
                    }
                    else
                    {
                        SetRouletteResult(Cost.COST_FIVE, i);
                    }
                    break;
                default:
                    break;
            }
        }

    }

    /// <summary>
    /// 更新した際の結果を表示する
    /// </summary>
    /// <param name="cost">表示モンスターコスト</param>
    /// <param name="GachaNum">どのショップ欄か</param>
    public void SetRouletteResult(Cost cost, int GachaNum)
    {
        var monsterDataBase = Resources.Load<MonsterDataBase>("ScriptableObject/MonsterDataBase");
        GachaPrice[GachaNum] = (int)cost;
        RandomValue = RandomPercent();
        switch (cost)
        {
            case Cost.COST_ONE:
                if (RandomValue <= 33)
                {
                    GachaImage[GachaNum].sprite = monsterDataBase.MonsterStatusList[(int)MonstersID.Slime].SelfGachaSprite;
                }
                else if (RandomValue <= 66)
                {
                    GachaImage[GachaNum].sprite = monsterDataBase.MonsterStatusList[(int)MonstersID.Mummy].SelfGachaSprite;
                }
                else
                {
                    GachaImage[GachaNum].sprite = monsterDataBase.MonsterStatusList[(int)MonstersID.Goblin].SelfGachaSprite;
                }
                break;
            case Cost.COST_TWO:
                if (RandomValue <= 33)
                {
                    GachaImage[GachaNum].sprite = monsterDataBase.MonsterStatusList[(int)MonstersID.Ghost].SelfGachaSprite;
                }
                else if (RandomValue <= 66)
                {
                    GachaImage[GachaNum].sprite = monsterDataBase.MonsterStatusList[(int)MonstersID.Mimic].SelfGachaSprite;
                }
                else
                {
                    GachaImage[GachaNum].sprite = monsterDataBase.MonsterStatusList[(int)MonstersID.SearchEye].SelfGachaSprite;
                }
                break;
            case Cost.COST_THREE:
                if (RandomValue <= 33)
                {
                    GachaImage[GachaNum].sprite = monsterDataBase.MonsterStatusList[(int)MonstersID.Monument].SelfGachaSprite;
                }
                else if (RandomValue <= 66)
                {
                    GachaImage[GachaNum].sprite = monsterDataBase.MonsterStatusList[(int)MonstersID.Soldier].SelfGachaSprite;
                }
                else
                {
                    GachaImage[GachaNum].sprite = monsterDataBase.MonsterStatusList[(int)MonstersID.Tetradon].SelfGachaSprite;
                }
                break;
            case Cost.COST_FOUR:
                if (RandomValue <= 50)
                {
                    GachaImage[GachaNum].sprite = monsterDataBase.MonsterStatusList[(int)MonstersID.KMachine].SelfGachaSprite;
                }
                else
                {
                    GachaImage[GachaNum].sprite = monsterDataBase.MonsterStatusList[(int)MonstersID.Silfied].SelfGachaSprite;
                }
                break;
            case Cost.COST_FIVE:
                GachaImage[GachaNum].sprite = monsterDataBase.MonsterStatusList[(int)MonstersID.Durahan].SelfGachaSprite;
                break;
        }
    }
    /// <summary>
    /// 買ったときの処理
    /// </summary>
    /// <param name="GachaNum">どのキャラを買ったか</param>
    public void Buy(int GachaNum)
    {
        int price = GachaPrice[GachaNum];
        var monsterDataBase = Resources.Load<MonsterDataBase>("ScriptableObject/MonsterDataBase");
        var summonCoaster = FieldManager.Instance.GetBlankAllMyCoaster();
        var currentPhase = PhaseManager.Instance.currentPhase;
        LoadKeys key = LoadKeys.None;
        if (currentPhase == PHASE.BATTLE)
        {
            key = LoadKeys.BattlePhase;
        }
        else if (GachaImage[GachaNum].sprite == soldOut)
        {
            key = LoadKeys.SoldOut;
        }
        else if (summonCoaster == null)
        {
            key = LoadKeys.BlankCoaster;
        }
        else if (PlayerGold < price)
        {
            key = LoadKeys.NoGold;
        }
        else
        {
            audioSource.PlayOneShot(BuySE);
            SetPlayerGold(-price);
            for (int i = 0; i < monsterDataBase.MonsterStatusList.Count; i++)
            {
                if (GachaImage[GachaNum].sprite == monsterDataBase.MonsterStatusList[i].SelfGachaSprite)
                {
                    GachaImage[GachaNum].sprite = soldOut;
                    GameObject instanceMonster = Instantiate(monsterDataBase.MonsterStatusList[i].SelfPrefab, summonCoaster.transform);
                    instanceMonster.GetComponent<Monster>().SetMonsterData(monsterDataBase.MonsterStatusList[i]);
                    instanceMonster.transform.localPosition = new Vector3(0, 0, 0);
                    InstanceMonsterManager.Instance.SetInstanceMonsterData(instanceMonster, (MonstersID)i, false);
                    break;
                }
            }
            buyCallBack?.Invoke();
            buyCallBack = null;
        }
        SpawnCantBuyText(key);   
    }
    /// <summary>
    /// 購入失敗時のテキストを表示
    /// </summary>
    /// <param name="loadKeys">表示するテキストの種類</param>
    void SpawnCantBuyText(LoadKeys loadKeys)
    {
        if (loadKeys == LoadKeys.None || cantBuyTextRoot.transform.childCount != 0) return;
        GameObject cantBuyText = Instantiate(Resources.Load<GameObject>(loadKey[(int)loadKeys]), cantBuyTextRoot.transform);
        cantBuyText.transform.localPosition = Vector3.zero;
        audioSource.PlayOneShot(CantBuySE);
    }

    /// <summary>
    /// レベルアップする時に呼ぶ
    /// </summary>
    public void Levelup()
    {
        switch (playerLevel)
        {
            case PlayerLevel.Lv1:
                playerLevel = PlayerLevel.Lv2;
                FieldManager.Instance.AddCoasters(AddCoaster);
                break;
            case PlayerLevel.Lv2:
                playerLevel = PlayerLevel.LV3;
                FieldManager.Instance.AddCoasters(AddCoaster);
                break;
            case PlayerLevel.LV3:
                Debug.Log("最高レベルです");
                return;
            default:
                break;
        }
        audioSource.PlayOneShot(LvUPSE);
    }
    /// <summary>
    /// フェイズ終了時に貰えるゴールドを付与
    /// </summary>
    private void GetPhaseEndGold() 
    {
        int GetGold = StageManager.Instance.CurrentStageData.Gold;
        if (PlayerGold >= 50)
        {
            GetGold += 5;
        }
        else if (PlayerGold >= 40)
        {
            GetGold += 4;
        }
        else if(PlayerGold >= 30)
        {
            GetGold += 3;
        }
        else if (PlayerGold >= 20)
        {
            GetGold += 2;
        }
        else if (PlayerGold >= 10) 
        {
            GetGold += 1;
        }
        SetPlayerGold(GetGold);
    }
    /// <summary>
    /// ランダムな0~100のfloat型変数を返す
    /// </summary>
    /// <returns>返した値</returns>
    public float RandomPercent()
    {
        return Random.Range(0, 100);
    }
    /// <summary>
    /// ステージクリア時に貰えるゴールドを付与
    /// </summary>
    /// <param name="clearGold"></param>
    public void GetStageClearGold(int clearGold)
    {
        SetPlayerGold(clearGold);
    }
}
