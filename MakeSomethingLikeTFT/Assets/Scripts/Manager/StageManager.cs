﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
///<summary>現在のステージや次のステージを管理する</summary>
public class StageManager : Singleton<StageManager>
{
    /// <summary>ステージデータリスト</summary>
    public List<StageData> StageDataBase { get; private set; } = new List<StageData>();
    /// <summary>現在のステージデータ</summary>
    public StageData CurrentStageData { get; private set; } = new StageData();
    /// <summary>現在のステージID</summary>
    public StageID CurrentStageID { get; private set; } = StageID.None;
    /// <summary>クリアやゲームオーバー時に表示するパネル</summary>
    private GameObject waitPanel = null;
    protected override void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(gameObject);
        StageDataBase = Resources.Load<StageDataBase>("ScriptableObject/StageDataBase").StageDataList;
    }
    /// <summary>
    /// 現在ステージを設定
    /// </summary>
    /// <param name="stageID">現在のステージ</param>
    public void SetStageData(StageID stageID)
    {        
        CurrentStageData = StageDataBase[(int)stageID];
        CurrentStageID = stageID;
        GameBGMManager.Instance.SetStageBGM(CurrentStageData.GetStageBGMList());
        GameObject.Find("BackGround").GetComponent <Image>().sprite = CurrentStageData.BackGround;
        PhaseManager.Instance.BeginStage();
        if (stageID == StageID.Tutorial)
        {
            
        }
    }
    /// <summary>
    /// クリア画面でキー入力待ちするコルーチン
    /// </summary>
    /// <param name="nextStageID">次のステージ</param>
    /// <returns></returns>
    public IEnumerator WaitForNext(StageID nextStageID)
    {
        var phaseManager = PhaseManager.Instance;
        phaseManager.StopUpdate();
        
        if (waitPanel == null)
        {
            GameObject clearPanelRoot = GameObject.Find("ClearPanelRoot");
            waitPanel = Instantiate(Resources.Load<GameObject>("UIPrefabs/StageClearPanel"), clearPanelRoot.transform);
            waitPanel.transform.localPosition = Vector3.zero;
            GameBGMManager.Instance.SetCurrentBGM(BGMs.Clear);
        }
        while (!Input.GetMouseButtonDown(0)) yield return null;
        if (nextStageID == StageID.CLEAR)
        {
            LocalSceneManager.Instance.SceneChange("Title", 3f);
            yield break;
        }       
        Destroy(waitPanel);
        SetStageData(nextStageID);
    }
    /// <summary>ゲームオーバー画面で入力待ちするコルーチン</summary>
    public IEnumerator WaitForGameOver()
    {
        var phaseManager = PhaseManager.Instance;
        phaseManager.StopUpdate();
        if (waitPanel == null)
        {
            GameObject clearPanelRoot = GameObject.Find("ClearPanelRoot");
            waitPanel = Instantiate(Resources.Load<GameObject>("UIPrefabs/GameOverPanel"), clearPanelRoot.transform);
            waitPanel.transform.localPosition = Vector3.zero;
            GameBGMManager.Instance.SetCurrentBGM(BGMs.GameOver);
        }
        while (!Input.GetKeyDown(KeyCode.Space)) yield return null;
        LocalSceneManager.Instance.SceneChange("Title", 3f);
    }
}
