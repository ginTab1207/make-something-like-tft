﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
/// <summary>ターンを管理する</summary>
public class TurnManager : Singleton<TurnManager>
{
    /// <summary>ターンリミット</summary>
    public int TurnLimit { private set; get; } = 2;
    /// <summary>現在のターン数</summary>
    public int currentTurn { private set; get; } = 0;
    /// <summary>現在のターンを表示するテキスト</summary>
    [SerializeField] Text turnText = null;
    /// <summary>ターンリミットを表示するテキスト</summary>
    [SerializeField] Text turnLimitText = null;
    /// <summary>ターン経過、ターンリミットであればTrueが返る</summary>
    public bool ProgressTurn()
    {
        currentTurn++;
        if (currentTurn > TurnLimit)//ターンリミットを超えたらゲームオーバー処理
        {
            var stageManager = StageManager.Instance;
            stageManager.StartCoroutine(stageManager.WaitForGameOver());
            InstanceEnemyManager.Instance.ResetEnemyObjs();
            TutorialManager.Instance.OnEndTutorialStage();
            return true;
        }
        turnText.text = "現在 " + currentTurn + " ターン目"; 
        return false;
    }
    /// <summary>ターンをリセットする(主にステージスタート時)</summary>
    public void ResetTurn()
    {
        currentTurn = 1;
        InstanceEnemyManager.Instance.SpawnEnemies();
        TurnLimit = StageManager.Instance.CurrentStageData.TurnLimit;
        turnLimitText.text = "ターン制限: " + TurnLimit + "ターン";
        turnText.text = "現在 " + currentTurn + " ターン目";
    }
}
