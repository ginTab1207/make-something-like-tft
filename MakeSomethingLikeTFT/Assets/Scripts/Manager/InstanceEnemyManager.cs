﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanceEnemyManager : Singleton<InstanceEnemyManager>
{
    public List<GameObject> instanceEnemyObjs { private set; get; } = new List<GameObject>();
    [SerializeField] GameObject EnemyRoot = null;
    //private string[] loadResouceKey = { "ScriptableObject/EnemyDataBase" };
    public void SetInstanceEnemys(GameObject instanceEnemy)
    {
        instanceEnemyObjs.Add(instanceEnemy);
    }
    public void ResetEnemyObjs()
    {
        instanceEnemyObjs.ForEach(x => Destroy(x));
        instanceEnemyObjs.Clear();        
    }
    public List<Enemy> GetBattleEnemies()
    {
        var BattleEnemies = new List<Enemy>();
        foreach (var enemy in instanceEnemyObjs)
        {
            BattleEnemies.Add(enemy.GetComponent<Enemy>());
        }
        return BattleEnemies;
    }
    public void SpawnEnemies()
    {
        var enemyDataBase = Resources.Load<EnemyDataBase>("ScriptableObject/EnemyDataBase");
        int[] numOfSpawnEnemies = StageManager.Instance.CurrentStageData.NumOfSpawnEnemies();
        for (int i = 0; i < numOfSpawnEnemies.Length; i++)
        {
            for (int o = 0; o < numOfSpawnEnemies[i]; o++)
            {
                GameObject instanceMons = Instantiate(enemyDataBase.EnemyDataList[i].SelfPrefab, EnemyRoot.transform);
                instanceMons.GetComponent<Enemy>().SetEnemyData(enemyDataBase.EnemyDataList[i]);
                SetInstanceEnemys(instanceMons);
            }
        }
    }
}
