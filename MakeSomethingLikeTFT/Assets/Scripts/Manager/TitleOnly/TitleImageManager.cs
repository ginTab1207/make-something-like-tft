﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class TitleImageManager : Singleton<TitleImageManager>
{
    private float currentAlpha = 1f;
    /// <summary>
    /// ここに入れたImageをFadeさせる
    /// </summary>
    /// <param name="images"></param>
    /// <param name="fade">fadeさせる値 1F毎にこの値がAlpha値に追加される(fadeOutなら負の値を入れる)</param>
    /// <param name="destroy">destroyするか,falseならSetActiveをfalseに</param>
    /// <returns></returns>
    public IEnumerator FadeImage(Image image, float fade, bool destroy,UnityAction callback)
    {
        while (image.color.a != 0f)
        {            
            currentAlpha = Mathf.Clamp(currentAlpha + fade, 0f, 1f);
            image.color = new Color(1f, 1f, 1f, currentAlpha);
            yield return null;
        }
        if (destroy) Destroy(image.gameObject);
        else image.gameObject.SetActive(false);
        Debug.Log("ImageFade完了");
        callback();
    }
    /// <summary>
    /// ここに入れたSpriteをFadeさせる
    /// </summary>
    /// /// <param name="fade">fadeさせる値 1F毎にこの値がAlpha値に追加される(fadeOutなら負の値を入れる)</param>
    public IEnumerator FadeSprite(SpriteRenderer spriteRenderer, float fade, bool destroy,UnityAction callback)
    {
        while (spriteRenderer.color.a != 0f)
        {
            currentAlpha = Mathf.Clamp(currentAlpha + fade, 0f, 1f);
            spriteRenderer.color = new Color(1f, 1f, 1f, currentAlpha);
            yield return null;
        }
        if (destroy) Destroy(spriteRenderer.gameObject);
        else spriteRenderer.gameObject.SetActive(false);
        Debug.Log("SpriteFade完了");
        callback();
    }
}
