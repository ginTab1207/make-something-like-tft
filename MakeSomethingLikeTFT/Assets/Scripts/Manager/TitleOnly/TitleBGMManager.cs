﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleBGMManager : MonoBehaviour
{
    private AudioSource m_audioSource = null;
    [SerializeField] AudioClip[] clips = null;
    private int currentClips = 0;
    private void Start()
    {
        m_audioSource = this.GetComponent<AudioSource>();
        m_audioSource.clip = clips[currentClips];
        m_audioSource.Play();
    }
    private void Update()
    {
        if (m_audioSource.isPlaying) return;
        
        if (currentClips < clips.Length - 1)
        {
            currentClips++;
        }
        else
        {
            currentClips = 0;
        }
        m_audioSource.clip = clips[currentClips];
        m_audioSource.Play();
    }

}
