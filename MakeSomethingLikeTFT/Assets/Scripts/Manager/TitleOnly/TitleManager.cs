﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TitleState 
{ 
    Animation,
    WaitClick,
}
/// <summary>
/// ・タイトルでの待機中アニメーションを流す
/// ・ゲームスタート時のステージを管理
/// </summary>
public class TitleManager : Singleton<TitleManager>
{
    private TitleState currentState = TitleState.Animation;
    [SerializeField] GameObject[] AnimObj = null;
    [SerializeField] GameObject[] WaitClickObj = null;
    [SerializeField] StageID nextStage = StageID.STAGE1;
    [SerializeField] float ChangeAnimTime = 10f;
    
    private float WaitClickStateTime = 0f;
    /// <summary>ゲームシーンに飛べるか</summary>
    private bool isChange = true;
    private void Update()
    {
        switch (currentState)
        {
            case TitleState.Animation:
                if (Input.GetMouseButtonDown(0))
                {
                    StateChange(TitleState.WaitClick);
                }
                break;
            case TitleState.WaitClick:
                WaitClickStateTime += Time.deltaTime;
                if (WaitClickStateTime >= ChangeAnimTime)
                {
                    StateChange(TitleState.Animation);
                }
                break;
            default:
                break;
        }
    }
    public void StateChange(TitleState state) 
    {
        switch (currentState)
        {
            case TitleState.Animation:
                foreach (var Obj in AnimObj)
                {
                    Obj.SetActive(false);
                }
                break;
            case TitleState.WaitClick:
                foreach (var Obj in WaitClickObj)
                {
                    Obj.SetActive(false);
                }
                break;
            default:
                break;
        }
        currentState = state;
        switch (state)
        {
            case TitleState.Animation:
                foreach (var Obj in AnimObj)
                {
                    Obj.SetActive(true);
                }
                break;
            case TitleState.WaitClick:
                foreach (var Obj in WaitClickObj)
                {
                    Obj.SetActive(true);
                }
                WaitClickStateTime = 0f;
                break;
            default:
                break;
        }
    }
    public void LoadGame()
    {
        if (currentState == TitleState.WaitClick && isChange)
        {
            AudioManager.Instance.OnEndTitle();
            LocalSceneManager.Instance.SceneChangeToGame(2f, nextStage);
            isChange = false;
        }
        else
        {
            return;
        }
    }
    
}
