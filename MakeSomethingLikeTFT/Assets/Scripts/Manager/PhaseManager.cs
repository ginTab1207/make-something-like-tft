﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>フェイズのID</summary>
public enum PHASE
{
    BUY,
    BATTLE,
    NONE
}
/// <summary>フェイズを管理する</summary>
public class PhaseManager : Singleton<PhaseManager>
{
    /// <summary>現在のフェイズ</summary>
    public PHASE currentPhase { private set; get; } = PHASE.NONE;
    /// <summary>カットインの親オブジェクト</summary>
    [SerializeField] GameObject CutInRoot = null;
    /// <summary>フェイズを表示するテキスト</summary>
    [SerializeField] Text PhaseText = null;
    /// <summary>残りフェイズ時間を表示するバー</summary>
    [SerializeField] Slider CountSlider = null;
    /// <summary>バーの画像</summary>
    [SerializeField] Image CountImage = null;
    [SerializeField,Tooltip("購入フェイズ時間")] float BuyPhaseTime = 20f;
    [SerializeField, Tooltip("バトルフェイズ時間")] float BattlePhaseTime = 20f;
    /// <summary>現在のフェイズ時間</summary>
    private float currentPhaseTime = 30f;
    /// <summary>バトルフェイズ開始時の関数を保持</summary>
    public delegate void onBeginBattle();
    /// <summary>購入フェイズ開始時の関数を保持</summary>
    public delegate void onBeginBuy();
    /// <summary>バトルフェイズ開始時の関数を入れる場所</summary>
    public event onBeginBattle OnBeginBattle = null;
    /// <summary>購入フェイズ開始時の関数を入れる場所</summary>
    public event onBeginBuy OnBeginBuy = null;
    /// <summary>購入フェイズ終了時の関数を入れる場所<</summary>
    public delegate void onEndBuy();
    /// <summary>バトルフェイズ終了時の関数を保持</summary>
    public delegate void onEndBattle();
    /// <summary>購入フェイズ終了時の関数を入れる場所</summary>
    public event onEndBuy OnEndBuy = null;
    /// <summary>バトルフェイズ終了時の関数を入れる場所<</summary>
    public event onEndBattle OnEndBattle = null;
    /// <summary>フェイズ処理を実行しているか</summary>
    public bool IsUpdate { get; private set; } = true;

    // Update is called once per frame
    private void Update()
    {
        if (!IsUpdate) return;
        currentPhaseTime -= Time.deltaTime;
        SetCountSlider();
        switch (currentPhase)
        {
            case PHASE.BUY:
                if (currentPhaseTime <= 0)
                {                   
                    OnEndPhase(PHASE.BATTLE);
                }
                break;
            case PHASE.BATTLE:
                if (currentPhaseTime <= 0)
                {
                    OnEndPhase(PHASE.BUY);
                }
                break;
            case PHASE.NONE:
                return;
            default:
                break;
        }
    }
    public void SkipPhase()
    {
        currentPhaseTime = 1f;
        SetCountSlider();
    }
    /// <summary>スライダーを残り時間と同期</summary>
    public void SetCountSlider()
    {
        CountSlider.value = currentPhaseTime;
    }
    /// <summary>
    /// テキスト等のUIを次のフェイズに対応させる
    /// </summary>
    /// <param name="nextPhase">次のフェイズ</param>
    public void SetPhaseUI(PHASE nextPhase)
    {
        PhaseText.text = nextPhase.ToString() + " PHASE";
        switch (currentPhase)
        {
            case PHASE.BUY:
                PhaseText.color = Color.yellow;
                CountImage.color = Color.yellow;
                break;
            case PHASE.BATTLE:
                PhaseText.color = Color.red;
                CountImage.color = Color.red;
                break;
            case PHASE.NONE:
                break;
            default:
                break;
        }
        SetCountSlider();
    }
    /// <summary>
    /// フェイズをセットする
    /// </summary>
    /// <param name="nextPhase">次のフェイズ</param>
    public void SetPhase(PHASE phase)
    {
        currentPhase = phase;
        SetPhaseUI(phase);
        switch (phase)
        {
            case PHASE.BUY:
                OnBeginBuy?.Invoke();
                CountSlider.maxValue = BuyPhaseTime;
                GameBGMManager.Instance.SetCurrentBGM(BGMs.Buy);
                Gacha.Instance.Roulette();
                currentPhaseTime = BuyPhaseTime;
                break;
            case PHASE.BATTLE:                
                OnBeginBattle?.Invoke();
                SynergyManager.Instance.ActivateSynergies();
                CountSlider.maxValue = BattlePhaseTime;
                GameBGMManager.Instance.SetCurrentBGM(BGMs.Battle);
                currentPhaseTime = BattlePhaseTime;                
                break;
            case PHASE.NONE:
                return;
        }        
        
    }
    /// <summary>
    /// フェイズが終わったことを知らせる
    /// </summary>
    /// <param name="nextPhase">次のフェイズ</param>
    public void OnEndPhase(PHASE nextPhase)
    {       
        switch (currentPhase)
        {
            case PHASE.BUY:
                OnEndBuy?.Invoke();
                CutIN(nextPhase);
                break;
            case PHASE.BATTLE:
                bool isTurnLimit = TurnManager.Instance.ProgressTurn();
                if (isTurnLimit)
                {
                    BattleManager.Instance.SetUpdate(false);
                    return;
                }
                else
                {
                    SynergyManager.Instance.DeActiveEffect();
                    OnEndBattle?.Invoke();
                    CutIN(nextPhase);
                }
                break;
            case PHASE.NONE:
                Debug.LogError("次のPhaseが設定されていません");
                return;
            default:
                break;
        }
        
        SetPhase(nextPhase);
    }
    /// <summary>フェイズ処理をストップ</summary>
    public void StopUpdate()
    {
        IsUpdate = false;
    }
    /// <summary>フェイズ処理をスタート</summary>
    public void StartUpdate()
    {
        IsUpdate = true;
    }
    /// <summary>ステージ開始時処理</summary>
    public void BeginStage()
    {
        StartUpdate();
        SetPhase(PHASE.BUY);
        TurnManager.Instance.ResetTurn();       
    }
    /// <summary>
    /// フェイズ変更時のカットインを挿入
    /// </summary>
    /// <param name="nextPhase">次のフェイズ</param>
    private void CutIN(PHASE nextPhase) 
    {
        switch (nextPhase)
        {
            case PHASE.BUY:
                Instantiate(Resources.Load<GameObject>("UIPrefabs/StartBuyCutIN"), CutInRoot.transform);
                break;
            case PHASE.BATTLE:
                Instantiate(Resources.Load<GameObject>("UIPrefabs/StartBattleCutIN"), CutInRoot.transform);
                break;
            case PHASE.NONE:
                break;
            default:
                break;
        }
    }
    
}

