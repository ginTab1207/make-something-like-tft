﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
/// <summary>
/// チュートリアルを管理
/// </summary>
public class TutorialManager : Singleton<TutorialManager>
{
    public bool IsTutorial { get; private set; } = false;
    private bool isNext = false;
    private TutorialExplain tutorialExplain = null;
    [SerializeField] GameObject tutorialPanelRoot = null;
    public string[] explainText = new string[5];
    private int textNumber = 0;
    private AudioSource audiosource = null;
    [SerializeField] AudioClip ClickSE = null;
    private void Start()
    {
        audiosource = GetComponent<AudioSource>();
    }
    public void StartTutorial()
    {
        if (PhaseManager.Instance.currentPhase == PHASE.BATTLE) return;
        IsTutorial = true;
        GameObject explain = Instantiate(Resources.Load<GameObject>("Tutorial/TutorialExplain"),tutorialPanelRoot.transform);
        tutorialExplain = explain.GetComponent<TutorialExplain>();
        tutorialExplain.SetText(explainText[textNumber]);
        switch (textNumber)
        {
            case 0:
                Gacha.Instance.buyCallBack = NextTutorial;
                break;
        }
        PhaseManager.Instance.StopUpdate();
        StartCoroutine(Tutorial());
    }
    private IEnumerator Tutorial()
    {
        while (IsTutorial)
        {
            while (!isNext) yield return null;
            if (explainText.Length > textNumber)
            {
                audiosource.PlayOneShot(ClickSE);
                
                PhaseManager.Instance.StartUpdate();
                isNext = false;
            }
            else
            {
                EndTutorial();
                break;
            }
            yield return new WaitForSeconds(0.3f);
        }      
    }
    private void EndTutorial()
    {      
        IsTutorial = false;   
    }
    public void NextTutorial()
    {
        isNext = true;
        PhaseManager.Instance.StartUpdate();
        Destroy(tutorialExplain.gameObject);
        switch (textNumber)
        {
            case 0:
                Gacha.Instance.buyCallBack = NextTutorial;
                break;
        }
    }
    public void OnEndTutorialStage()
    {
        InstanceMonsterManager.Instance.AllDeleteMonster();
        Gacha.Instance.Reset();
        ItemManager.Instance.AllDeleteItem();
    }
    public void DebugSkipTutorial()
    {
        if (!DebugMode.Instance.GetIsDebug()) return;
        IsTutorial = false;
    }
}
