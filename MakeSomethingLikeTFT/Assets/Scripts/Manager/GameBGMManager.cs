﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum BGMs
{
    Buy,
    Battle,
    Clear,
    GameOver,
    None
}
public class GameBGMManager : Singleton<GameBGMManager>
{
    [SerializeField] AudioSource audioSource = null;
    public AudioClip[] audioClips = new AudioClip[(int)BGMs.None];
    public void SetStageBGM(AudioClip[] clips)
    {
        audioClips = clips;
    }
    public void SetCurrentBGM(BGMs bgm)
    {
        audioSource.clip = audioClips[(int)bgm];
        audioSource.Play();
    }    
}
