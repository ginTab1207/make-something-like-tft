﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum ItemID
{
    Spring,
    EmptyCan,
    Screw,
    None
}
public class ItemManager : Singleton<ItemManager>
{
    List<ItemData> m_itemDataList = new List<ItemData>();
    public List<GameObject> ItemList { private set; get; } = new List<GameObject>();
    public List<GameObject> ItemCoasterList { private set; get; } = new List<GameObject>();
    private void Start()
    {
        m_itemDataList = Resources.Load<ItemDataBase>("ScriptableObject/ItemDataBase").ItemDataList;
        ItemCoasterList.AddRange(GameObject.FindGameObjectsWithTag("ItemCoaster"));
    }
    /// <summary>アイテムを生成 </summary>
    /// <param name="number">個数</param>
    public void GenerateItem(ItemID key,int number)
    {
        for (int i = 0; i < number; i++)
        {
            var blankCoaster = GetBlankItemCoaster();
            if (blankCoaster!=null)
            {
                GameObject clone = Instantiate(m_itemDataList[(int)key].ItemPrefab, blankCoaster.transform);
                ItemList.Add(clone);
            }            
        }
    }
    public GameObject GetBlankItemCoaster()
    {
        foreach (var coaster in ItemCoasterList)
        {
            if (coaster.transform.childCount == 0)
            {
                return coaster;
            }
        }
        Debug.LogWarning("アイテム欄に空きがありません");
        return null;
        
    }
    public void AllDeleteItem()
    {
        List<GameObject> copyList = new List<GameObject>(ItemList);
        foreach (var item in copyList)
        {
            Destroy(item);
        }
        ItemList.Clear();
    }
}
