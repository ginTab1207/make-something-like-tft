﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplainBoardManager : Singleton<ExplainBoardManager>
{
    [SerializeField] GameObject explainBoardRoot = null;
    private const float boardOffSet = 2f;
    private List<ExplainBoard> instanceBoards = new List<ExplainBoard>();

    public void SetExplainBoard(GameObject monsterObj)
    {
        GameObject Board = Instantiate(Resources.Load<GameObject>("UIPrefabs/ExplainBoard"),explainBoardRoot.transform);
        Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Board.transform.position = new Vector3(pos.x + boardOffSet, pos.y, 10f);
        Monster monster = monsterObj.GetComponent<Monster>();
        var instanceBoard = Board.GetComponent<ExplainBoard>();
        instanceBoards.Add(instanceBoard);
        var synImage = SynergyManager.Instance.SynergyImages[(int)monster.m_MonsterData.Synergy];
        instanceBoard.SetBoard(monster.CurrentHP.ToString(),monster.CurrentAttackPower.ToString(),monster.m_SkillData.SkillName.ToString(), monster.m_SkillData.SkillExplain, synImage, monster.m_MonsterData.Synergy.ToString());
        StartCoroutine(WaitRightClick());
    }    
    IEnumerator WaitRightClick()
    {
        yield return new WaitForSeconds(0.5f);
        while (!Input.GetMouseButtonDown(1)) { yield return null; }
        Debug.Log("rightClick");
        instanceBoards.ForEach(x => Destroy(x.gameObject));
        instanceBoards.Clear();
    }
    
}
