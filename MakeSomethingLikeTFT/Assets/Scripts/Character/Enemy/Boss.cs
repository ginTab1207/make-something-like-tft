﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : Enemy
{
    protected override void BattleUpdate(float deltaTime)
    {
        if (!m_isBattle || PhaseManager.Instance.currentPhase != PHASE.BATTLE) return;
        if (m_statusEffect[(int)StatusEffect.Stone]) return;
        currentAttackCoolTime += deltaTime * m_EnemyData.DefaultAttackSpeed;
        if (currentAttackCoolTime >= BattleManager.EnemyAttackTime)
        {
            int random = Random.Range(0, 2);
            switch (random)
            {
                case 0:
                    m_animator.SetTrigger("Attack1");
                    break;
                case 1:
                    m_animator.SetTrigger("Attack2");
                    break;
                case 2:
                    m_animator.SetTrigger("Attack3");
                    break;
            }
            var monsters = BattleManager.Instance.GetRandomFightingMonster(m_numOfAttacks);
            if (monsters != null)
            {
                Attack(monsters);
            }
            currentAttackCoolTime = 0f;
        }
    }
    public override bool TakeDamage(float damage)
    {
        m_animator.SetTrigger("TakeHit");
        bool isDead = base.TakeDamage(damage);
        if (CurrentHP < m_EnemyData.MaxHp * (angryPercent / 100) && !m_statusEffect[(int)StatusEffect.Angry])
        {
            SetStatusEffect(StatusEffect.Angry, 100000f);
            m_numOfAttacks = 3;
        }
        return isDead;
    }
    public override void Dead()
    {
        InstanceEnemyManager.Instance.instanceEnemyObjs.Remove(gameObject);
        BattleManager.Instance.RemoveListEnemy(this);
        m_animator.SetTrigger("Death");
        base.Dead();
    }
}
