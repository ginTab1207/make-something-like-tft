﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : CharacterBase
{
    public EnemyData m_EnemyData { private set; get; } = new EnemyData();
    [SerializeField] protected Text m_nameText = null;
    [SerializeField,Tooltip(" 怒り出す体力　0で怒らない")] protected float angryPercent = 0f;

    protected override void BattleUpdate(float deltaTime)
    {
        if(!m_isBattle || PhaseManager.Instance.currentPhase != PHASE.BATTLE) return;
    }
    public void SetEnemyData(EnemyData data)
    {
        if (data == null) return;
        m_EnemyData = new EnemyData();
        m_EnemyData = data;
        m_numOfAttacks = m_EnemyData.NumOfAttacks;
        CurrentEvasiveness = m_EnemyData.Evasiveness;
        m_nameText.text = m_EnemyData.EnemyName.ToString();
        CurrentHP = m_EnemyData.MaxHp;
        CurrentAttackPower = m_EnemyData.DefaultPower;
        m_hpSlider.maxValue = CurrentHP;
        m_hpSlider.value = CurrentHP;
        BattleManager.Instance.AddListEnemy(this);
    }
    
    
    protected void Attack(List<Monster> monsters)
    {
        foreach (var monster in monsters)
        {
            if (monster == null) return; 
            monster.TakeDamage(CurrentAttackPower + GetStatusEffectPower());
        }
    }   
    protected override void onBeginBattle()
    {        
        m_isBattle = true;        
    }

    protected override void onEndBattle()
    {
        m_isBattle = false;
        AllClearStatusEffect();
    }
}
