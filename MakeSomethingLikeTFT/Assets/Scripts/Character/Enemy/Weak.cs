﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weak : Enemy
{
    protected override void BattleUpdate(float deltaTime)
    {
        if (!m_isBattle) return;
        if (m_statusEffect[(int)StatusEffect.Stone]) return;
        currentAttackCoolTime += deltaTime * m_EnemyData.DefaultAttackSpeed;
        if (currentAttackCoolTime >= BattleManager.EnemyAttackTime)
        {
            m_animator.SetTrigger("Attack");
            var monsters = BattleManager.Instance.GetRandomFightingMonster(m_numOfAttacks);
            if (monsters != null)
            {
                Attack(monsters);
            }            
            currentAttackCoolTime = 0f;
        }
    }
    public override bool TakeDamage(float damage)
    {
        m_animator.SetTrigger("Hurt");
        bool isDead = base.TakeDamage(damage);
        if (CurrentHP < m_EnemyData.MaxHp * (angryPercent / 100))
        {
            SetStatusEffect(StatusEffect.Angry, 100000f);
        }
        return isDead;
    }
    public override void Dead()
    {
        InstanceEnemyManager.Instance.instanceEnemyObjs.Remove(gameObject);
        BattleManager.Instance.RemoveListEnemy(this);
        m_animator.SetTrigger("Death");
        base.Dead();
    }
}
