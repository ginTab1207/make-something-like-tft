﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Monster : CharacterBase
{
    public MonsterData m_MonsterData { private set; get; } = new MonsterData();
    ///<summary>スキル系(EffectPower,CoolDown)は今のところ可変予定がないのでCurrentに落とさずScriptableのDataを呼び出す形で使っている</summary>
    public SkillData m_SkillData { private set; get; } = new SkillData();
    protected Collider2D m_Collider = null;
    protected float currentSkillCoolTime = 0f;
    protected float passiveTime = 0f;
    
    private void Start()
    {
    }
    public void SetMonsterData(MonsterData data)
    {
        if (data == null) return;
        m_MonsterData = new MonsterData();
        m_MonsterData = data;
        CurrentEvasiveness = m_MonsterData.Evasiveness;
        CurrentHP = m_MonsterData.MaxHp;
        CurrentAttackSpeed = m_MonsterData.DefaultAttackSpeed;
        CurrentAttackPower = m_MonsterData.DefaultPower;
        m_hpSlider.maxValue = CurrentHP;
        m_hpSlider.value = CurrentHP;
        m_SkillData = m_MonsterData.GetSkill();
    }
    protected override void BattleUpdate(float deltaTime)
    {
        if (!m_isBattle) return;
        //----攻撃処理----
        currentAttackCoolTime += deltaTime * CurrentAttackSpeed;
        if (currentAttackCoolTime >= BattleManager.MonsterAttackTime)
        {
            StartCoroutine(AttackAnimation(transform.localPosition, 0.6f));
            var enemies = BattleManager.Instance.GetRandomFightingEnemies(m_numOfAttacks);
            if (enemies != null)
            {
                Attack(enemies);
            }            
            currentAttackCoolTime = 0f;
        }
        //-----スキル処理-----
        if (m_SkillData.SkillType == SkillTypes.Passive) return;
        currentSkillCoolTime += deltaTime;
        if (currentSkillCoolTime >= m_SkillData.CoolDown)
        {
            StartCoroutine(SkillAnimation(0.6f));
            UseActiveSkill();
            currentSkillCoolTime = 0f;
        }
    }

    protected virtual void Attack(List<Enemy> enemies)
    {
        foreach (var enemy in enemies)
        {
            if (enemy == null) return;
            enemy.TakeDamage(CurrentAttackPower + GetStatusEffectPower());
        }
    }
    public override void Dead()
    {
        InstanceMonsterManager.Instance.instanceMonsterObjs.Remove(gameObject);
        BattleManager.Instance.RemoveListMons(this);      
        base.Dead();
    }
    protected override void onBeginBattle()
    {
        if (gameObject.transform.parent.CompareTag("FieldCoaster"))
        {
            m_isBattle = true;
            currentSkillCoolTime = 0f;
            currentAttackCoolTime = 0f;
            BattleManager.Instance.AddListMons(this);
            if (m_SkillData.SkillType == SkillTypes.Passive || m_SkillData.SkillType == SkillTypes.Special)
            {
                StartCoroutine(SkillAnimation(0.6f));
                StartCoroutine(SetPassiveSkill(m_SkillData.EffectTime));
            }
        }
    }
    protected override void onEndBattle()
    {
        BattleManager.Instance.RemoveListMons(this);
        m_isBattle = false;
        AllClearStatusEffect();
    }
    
    protected virtual void UseActiveSkill()
    {

    }
    protected virtual IEnumerator SetPassiveSkill(float time)
    {
        yield break;
    }
    protected virtual IEnumerator AttackAnimation(Vector3 pos, float time)
    {
        if (!m_isBattle) yield break;
        for (int i = 0; i < time * 60; i++)
        {
            transform.localPosition += Vector3.up * 1.5f;
            yield return null;
        }
        transform.localPosition = pos;
    }
    protected virtual IEnumerator SkillAnimation(float time)
    {
        if (!m_isBattle) yield break;
        for (int i = 0; i < time * 60; i++)
        {
            if (i <= (time * 60) / 2) transform.localScale += (Vector3.up + Vector3.right) * 0.1f;
            else transform.localScale += (Vector3.down + Vector3.left) * 0.1f;            
            yield return null;
        }
        transform.localScale = Vector3.one;
    }
    public void SELL()
    {
        Gacha.Instance.SetPlayerGold((int)m_MonsterData.Price);
        Dead();
        
    }
}
