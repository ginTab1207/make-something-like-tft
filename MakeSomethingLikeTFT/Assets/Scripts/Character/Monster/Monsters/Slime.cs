﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slime : Monster
{
    protected override void Attack(List<Enemy> enemies)
    {
        foreach (var enemy in enemies)
        {
            if (enemy == null) return;
            bool isDead = enemy.TakeDamage(CurrentAttackPower + GetStatusEffectPower());
            if (!isDead)
            {
                enemy.SetStatusEffect(StatusEffect.Adhesive, m_SkillData.EffectTime);
            }
        }
    }
}
