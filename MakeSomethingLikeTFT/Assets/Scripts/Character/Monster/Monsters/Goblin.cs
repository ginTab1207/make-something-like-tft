﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goblin : Monster
{
    protected override void Attack(List<Enemy> enemies)
    {
        base.Attack(enemies);
    }
    protected override void UseActiveSkill()
    {
        ChangeAttackPower(m_SkillData.EffectPower);
    }
}
