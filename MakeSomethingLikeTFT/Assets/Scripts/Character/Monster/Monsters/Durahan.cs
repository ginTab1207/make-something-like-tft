﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Durahan : Monster
{    
    protected override void Attack(List<Enemy> enemies)
    {
        foreach (var enemy in enemies)
        {
            if (enemy == null) return;
            bool isDead = enemy.TakeDamage(CurrentAttackPower + GetStatusEffectPower());
            if (isDead) TakeHeal(m_SkillData.EffectPower);
        }
    }
    protected override void UseActiveSkill()
    {
        ChangeAttackPower(m_SkillData.EffectPower);
    }
}
