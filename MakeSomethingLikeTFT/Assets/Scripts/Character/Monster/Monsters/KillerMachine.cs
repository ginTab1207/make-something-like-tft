﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillerMachine : Monster
{
    private int currentAttacks = 0;
    protected override void Attack(List<Enemy> enemies)
    {
        foreach (var enemy in enemies)
        {
            if (enemy == null) return;
            currentAttacks++;
            if (currentAttacks % 2 == 0)
            {
                enemy.TakeDamage(m_SkillData.EffectPower);       
            }
            enemy.TakeDamage(CurrentAttackPower + GetStatusEffectPower());
        }
    }
}
