﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ghost : Monster
{
    protected override void Attack(List<Enemy> enemies)
    {
        base.Attack(enemies);
    }
    protected override void UseActiveSkill()
    {
        Enemy horroredEnemy = BattleManager.Instance.GetNearestFightingEnemy(gameObject.transform.position);
        horroredEnemy.SetStatusEffect(StatusEffect.Horror,m_SkillData.EffectTime);
    }
}
