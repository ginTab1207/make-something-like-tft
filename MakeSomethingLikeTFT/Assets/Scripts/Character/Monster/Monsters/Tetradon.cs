﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tetradon : Monster
{
    protected override void Attack(List<Enemy> enemies)
    {
        base.Attack(enemies);
    }
    protected override void UseActiveSkill()
    {
        if (!m_isBattle) return;
        List<Enemy> enemies = BattleManager.Instance.fightingEnemies;
        if (enemies == null)
        {
            return;
        }
        foreach (var enemy in enemies)
        {
            enemy.TakeDamage(m_SkillData.EffectPower);
        }
    }
}
