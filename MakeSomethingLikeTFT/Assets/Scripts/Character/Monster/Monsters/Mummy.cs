﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mummy : Monster
{
    protected override void Attack(List<Enemy> enemies)
    {
        base.Attack(enemies);
    }
    protected override void UseActiveSkill()
    {
        List<Monster> healMonsters = BattleManager.Instance.GetRandomFightingMonster(m_SkillData.NumOfTargets);
        if (healMonsters == null) return;
        foreach (var monster in healMonsters)
        {
            monster.TakeHeal(m_SkillData.EffectPower);
        }
    }
}
