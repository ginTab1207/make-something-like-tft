﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monument : Monster
{
    protected override void Attack(List<Enemy> enemies)
    {
        base.Attack(enemies);
    }
    protected override IEnumerator SetPassiveSkill(float time)
    {
        yield return new WaitForSeconds(0.3f);
        SetStatusEffect(StatusEffect.SlowStarter, time);
    }
}
