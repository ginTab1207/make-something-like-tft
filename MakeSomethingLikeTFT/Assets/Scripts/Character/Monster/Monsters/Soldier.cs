﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soldier : Monster
{
    protected override void Attack(List<Enemy> enemies)
    {
        base.Attack(enemies);
    }
    protected override IEnumerator SetPassiveSkill(float time)
    {
        yield return new WaitForSeconds(0.3f);
        List<Monster> powerUpMons = BattleManager.Instance.GetRandomFightingMonster(m_SkillData.NumOfTargets);
        if (powerUpMons == null) yield break;
        foreach (var monster in powerUpMons)
        {
            monster.ChangeAttackPower(m_SkillData.EffectPower);
        }        
    }
}
