﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchEye : Monster 
{
    protected override void Attack(List<Enemy> enemies)
    {
        base.Attack(enemies);
    }
    protected override IEnumerator SetPassiveSkill(float time)
    {
        yield return new WaitForSeconds(0.3f);
        Enemy enemy = BattleManager.Instance.GetNearestFightingEnemy(gameObject.transform.position);
        enemy.SetStatusEffect(StatusEffect.Stone, time);
        yield return new WaitForSeconds(time);
    }
}
