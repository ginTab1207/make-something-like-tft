﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Silfied : Monster
{
    [SerializeField] float useSkillPercentForEnemies = 50f;
    protected override void Attack(List<Enemy> enemies)
    {
        base.Attack(enemies);
    }
    protected override void UseActiveSkill()
    {
        var battleManager = BattleManager.Instance;
        if (Random.Range(0, 100) <= useSkillPercentForEnemies)
        {
            List<Enemy> enemies = battleManager.GetRandomFightingEnemies(m_SkillData.NumOfTargets);
            if (enemies == null) return;
            foreach (var enemy in enemies)
            {
                enemy.TakeDamage(m_SkillData.EffectPower);
            }
        }
        else
        {
            List<Monster> monsters = battleManager.GetRandomFightingMonster(m_SkillData.NumOfTargets);
            if (monsters == null) return;
            foreach (var monster in monsters)
            {
                monster.TakeDamage(m_SkillData.EffectPower);
            }
        }
    }
}
