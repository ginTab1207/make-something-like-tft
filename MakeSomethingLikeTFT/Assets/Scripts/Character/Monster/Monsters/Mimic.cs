﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mimic : Monster
{
    protected override void Attack(List<Enemy> enemies)
    {   
        base.Attack(enemies);
    }
    protected override IEnumerator SetPassiveSkill(float time)
    {
        yield return new WaitForSeconds(0.3f);
        CurrentAttackPower = m_SkillData.EffectPower;
        yield return new WaitForSeconds(time);
        CurrentAttackPower = m_MonsterData.DefaultPower;
    }
}
