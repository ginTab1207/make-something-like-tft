﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// キャラクターの継承元
/// </summary>
public abstract class CharacterBase : MonoBehaviour
{
    [SerializeField] AudioSource m_audioSource = null;
    [SerializeField] protected Animator m_animator = null;
    [SerializeField] protected Slider m_hpSlider = null;
    /// <summary>
    /// 現在のHP
    /// </summary>
    public float CurrentHP { get; protected set; } = 0;
    /// <summary>
    /// 現在の攻撃力
    /// </summary>
    public float CurrentAttackPower { get; protected set; } = 0;
    /// <summary>
    /// 現在の攻撃スピード
    /// </summary>
    public float CurrentAttackSpeed { get; protected set; } = 0;
    /// <summary> 現在の回避率</summary>
    public float CurrentEvasiveness { get; protected set; } = 0f;
    /// <summary>
    /// 攻撃回数
    /// </summary>
    public int m_numOfAttacks { get; protected set; } = 1;  
    /// <summary>
    /// 次回攻撃までの時間
    /// </summary>
    protected float currentAttackCoolTime = 0f;
    /// <summary>
    /// 戦闘状態か否か
    /// </summary>
    public bool m_isBattle { get; protected set; } = false;
    /// <summary>
    /// オーラ(エフェクト)オブジェクト
    /// </summary>
    private GameObject Aura = null;
    /// <summary>
    /// 保持している状態異常
    /// </summary>
    protected bool[] m_statusEffect; //2進数で状態異常管理したい
    /// <summary>
    /// 死ぬまでのラグ
    /// </summary>
    public static float DeadTime = 0.02f;
    protected void Awake()
    {
        m_statusEffect = new bool[System.Enum.GetNames(typeof(StatusEffect)).Length];
        PhaseManager.Instance.OnBeginBattle += onBeginBattle;
        PhaseManager.Instance.OnEndBattle += onEndBattle;
        m_audioSource.volume = AudioManager.Instance.AudioVolume;
    }
    private protected void Update()
    {
        BattleUpdate(Time.deltaTime);
    }
    protected abstract void BattleUpdate(float deltaTime);
    /// <summary>ダメージを受けた時 </summary>
    /// <param name="damage">受けたダメージ量</param>
    /// <returns>倒されたかどうか</returns>
    public virtual bool TakeDamage(float damage)
    {
        if (HitCheck(CurrentEvasiveness)) return false; //躱した
        DamageTextManager.Instance.InstanceDamageText(damage, gameObject.transform.position);
        GameObject particle = Instantiate(Resources.Load<GameObject>("ParticleSystems/DamageParticle"), gameObject.transform);
        particle.transform.localPosition = new Vector3(0, 0, -35);
        PlayBattleSE(BattleSE.Damage);
        CurrentHP = Mathf.Clamp(CurrentHP - damage, 0, CurrentHP);
        m_hpSlider.value = CurrentHP;
        if (CurrentHP <= 0)
        {
            Dead();
            return true;
        }
        else return false;
    }
    /// <summary>
    /// ヒールを受けた時
    /// </summary>
    /// <param name="heal">受けた回復量</param>
    public void TakeHeal(float heal) 
    {
        if (gameObject == null) return;
        GameObject particle = Instantiate(Resources.Load<GameObject>("ParticleSystems/HealParticle"), gameObject.transform);
        particle.transform.localPosition = new Vector3(0,0,-35);
        PlayBattleSE(BattleSE.Heal);
        CurrentHP = Mathf.Clamp(CurrentHP + heal, 0, m_hpSlider.maxValue);
        m_hpSlider.value = CurrentHP;
    }
    /// <summary>
    /// 攻撃力を変える時
    /// </summary>
    /// <param name="changePower">変える攻撃値</param>
    public void ChangeAttackPower(float changePower)
    {
        CurrentAttackPower = Mathf.Clamp(CurrentAttackPower + changePower, 0, float.MaxValue);
    }
    /// <summary>
    /// 回避力を変える時
    /// </summary>
    /// <param name="changePower">変える回避値</param>
    public void ChangeEvansiveness(float changeEvan)
    {
        CurrentEvasiveness = Mathf.Clamp(CurrentEvasiveness + changeEvan, 0, 100);
    }
    /// <summary>
    /// 攻撃スピードを変える時
    /// </summary>
    /// <param name="changePower">変える攻撃スピード値</param>
    public void ChangeAttackSpeed(float changeAS)
    {
        CurrentAttackSpeed = Mathf.Clamp(CurrentAttackSpeed + changeAS, 0, float.MaxValue);
    }
    /// <summary>
    /// 戦闘が始まった時の関数(デリゲート管理)
    /// </summary>
    protected abstract void onBeginBattle();
    /// <summary>
    /// 戦闘が終わった時の関数(デリゲート管理)
    /// </summary>
    protected abstract void onEndBattle();
    /// <summary>
    /// 倒された時
    /// </summary>
    public virtual void Dead() 
    {
        PhaseManager.Instance.OnBeginBattle -= onBeginBattle;
        PhaseManager.Instance.OnEndBattle -= onEndBattle;
        Destroy(Aura);
        Destroy(this.gameObject, DeadTime);
    }
    /// <summary>
    /// 戦闘中に鳴らすサウンド
    /// </summary>
    /// <param name="SEname">鳴らすサウンド名</param>
    public void PlayBattleSE(BattleSE SEname)
    {
        var battleManager = BattleManager.Instance;
        switch (SEname)
        {
            case BattleSE.Damage:
                m_audioSource.PlayOneShot(battleManager.damageSE);
                break;
            case BattleSE.Heal:
                m_audioSource.PlayOneShot(battleManager.healSE);
                break;
            case BattleSE.None:
                return;
        }
    }
    /// <summary>
    /// 状態異常をセットする
    /// </summary>
    /// <param name="statusEffect">状態異常名</param>
    /// <param name="time">状態異常の時間</param>
    public void SetStatusEffect(StatusEffect statusEffect, float time)
    {
        if (statusEffect == StatusEffect.Angry)
        {
            Aura = Instantiate(Resources.Load<GameObject>("ParticleSystems/AngryAura"), gameObject.transform);
            Aura.transform.localPosition = new Vector3(0, 0, -35);
        }
        m_statusEffect[(int)statusEffect] = true;
        StartCoroutine(StatusEffectTime(statusEffect, time));
    }
    /// <summary>
    /// 状態異常を消す
    /// </summary>
    /// <param name="statusEffect">消す状態異常</param>
    protected void ClearStatusEffect(StatusEffect statusEffect)
    {
        m_statusEffect[(int)statusEffect] = false;
    }
    /// <summary>
    /// 全ての状態異常を消す
    /// </summary>
    protected void AllClearStatusEffect()
    {
        for (int i = 0; i < m_statusEffect.Length; i++)
        {
            if ((StatusEffect)i == StatusEffect.Angry) continue;
            m_statusEffect[i] = false;
        }
    }
    /// <summary>
    /// 戦闘状態かを変更する
    /// </summary>
    /// <param name="b">変える戦闘状態先(trueなら戦闘中)</param>
    public void SetIsBattle(bool b)
    {
        m_isBattle = b;
    }
    /// <summary>
    /// 状態異常の時間を管理するコルーチン
    /// </summary>
    /// <param name="statusEffect">状態異常名</param>
    /// <param name="time">状態異常時間</param>
    /// <returns>状態異常時間が返る</returns>
    protected IEnumerator StatusEffectTime(StatusEffect statusEffect, float time)
    {
        if (m_statusEffect[(int)statusEffect] == false) yield break;
        yield return new WaitForSeconds(time);
        ClearStatusEffect(statusEffect);
    }
    /// <summary>
    /// 攻撃力が変わる状態異常を取得する
    /// </summary>
    /// <returns>変わる攻撃値</returns>
    protected float GetStatusEffectPower()
    {
        float adjustPower = 0f;
        if (m_statusEffect[(int)StatusEffect.Adhesive])　
        {
            adjustPower -= 1f;
        }
        if (m_statusEffect[(int)StatusEffect.Horror])
        {
            adjustPower -= 2f;
        }
        if (m_statusEffect[(int)StatusEffect.SlowStarter])
        {
            adjustPower -= 5f;
        }
        if (m_statusEffect[(int)StatusEffect.Angry])
        {
            adjustPower += 2f;
        }
        return adjustPower;        
    }
    /// <summary>
    /// 回避値の判定を行い攻撃が当たったか否かを確認する
    /// </summary>
    /// <param name="Evasiveness">回避値</param>
    /// <returns>当たったか否か</returns>
    public bool HitCheck(float Evasiveness)
    {
        int random = Random.Range(1, 100);
        if (random <= Evasiveness) return true;
        else return false;
    }
    
}
