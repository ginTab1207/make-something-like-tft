﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum MonstersID
{
    Slime,
    Mummy,
    Goblin,
    Ghost,
    Mimic,
    SearchEye,
    Monument,
    Soldier,
    Tetradon,
    KMachine,
    Silfied,
    Durahan,
    EvoSlime,
    EvoMummy,
    EvoGoblin,
    EvoGhost,
    EvoMimic,
    EvoSearchEye,
    EvoMonument,
    EvoSoldier,
    EvoTetradon,
    EvoKMachine,
    EvoSilfied,
    EvoDurahan,
    None
}
[CreateAssetMenu(fileName = "MonsterDataBase", menuName = "ScriptableObjects/MonsterData", order = 1)]
public class MonsterDataBase : ScriptableObject
{
    public List<MonsterData> MonsterStatusList = new List<MonsterData>();   
}
[Serializable]
public class MonsterData
{    
    [SerializeField]
    MonstersID monsterName = MonstersID.None;
    [SerializeField]
    SynergyID synergy = SynergyID.None;
    [SerializeField]
    float maxHP = 4;
    [SerializeField]
    float defaultPower = 1;
    [SerializeField]
    float defaultAttackSpeed = 1f;
    [SerializeField]
    float price = 0;
    [SerializeField]
    float evasiveness = 0f;
    [SerializeField]
    Sprite selfSprite = null;
    [SerializeField]
    Sprite selfGachaSprite = null;
    [SerializeField]
    GameObject selfPrefab = null;

    public MonstersID MonsterName => monsterName;
    public SynergyID Synergy => synergy;
    public float MaxHp => maxHP;
    public float DefaultPower => defaultPower;
    public float DefaultAttackSpeed => defaultAttackSpeed;
    public float Price => price;
    public float Evasiveness => evasiveness;
    public Sprite SelfSprite => selfSprite;
    public Sprite SelfGachaSprite => selfGachaSprite;
    public GameObject SelfPrefab => selfPrefab;
    public SkillData GetSkill()
    {
        SkillData skillData = Resources.Load<SkillDataBase>("ScriptableObject/SkillDataBase").SkillDataList[(int)monsterName];
        return skillData;
    }
}
