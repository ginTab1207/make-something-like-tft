﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum EnemyID
{
    Weak,
    Normal,
    Strong,
    Boss,
}

[CreateAssetMenu(fileName = "EnemyDataBase", menuName = "ScriptableObjects/EnemyData", order = 1)]
public class EnemyDataBase : ScriptableObject
{
    public List<EnemyData> EnemyDataList = new List<EnemyData>();
}
[Serializable]
public class EnemyData
{    
    [SerializeField]
    EnemyID enemyName = default;
    [SerializeField]
    float maxHP = 0;
    [SerializeField]
    float defaultPower = 0;
    [SerializeField]
    float defaultAttackSpeed = 1f;
    [SerializeField,Tooltip("回避率")]
    float evasiveness = 0f;
    [SerializeField]
    GameObject selfPrefab = null;
    [SerializeField,Tooltip("攻撃する人数")]
    int numOfAttacks = 1;
    public EnemyID EnemyName => enemyName;
    public float MaxHp => maxHP;
    public float DefaultPower => defaultPower;
    public float DefaultAttackSpeed => defaultAttackSpeed;
    public float Evasiveness => evasiveness;
    public GameObject SelfPrefab => selfPrefab;
    public int NumOfAttacks => numOfAttacks;
}
