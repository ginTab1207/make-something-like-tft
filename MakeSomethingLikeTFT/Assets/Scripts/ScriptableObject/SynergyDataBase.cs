﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "SynergyDataBase", menuName = "ScriptableObjects/SynergyData", order = 1)]
public class SynergyDataBase : ScriptableObject
{
    public List<SynergyData> SynergyDataList = new List<SynergyData>();
}
[Serializable]
public class SynergyData
{
    [SerializeField, Tooltip("シナジー名")]
    SynergyID synergy = SynergyID.None;
    [SerializeField,Tooltip("シナジー発動に必要なモンスター数")]
    int[] requiredMonsters = default;
    [SerializeField, Tooltip("このシナジーのエフェクトID")]
    SynergyEffectID[] synergyEffectIDs = default;

    public SynergyID Synergy => synergy;
    /// <summary>シナジー発動に必要なモンスター数</summary>
    /// <param name="synergyLevel">シナジーの強さ 0から始まる</param>
    /// <returns></returns>
    public int[] RequiredMonster => requiredMonsters;
    public SynergyEffectID[] SynergyEffectIDs => synergyEffectIDs;
}