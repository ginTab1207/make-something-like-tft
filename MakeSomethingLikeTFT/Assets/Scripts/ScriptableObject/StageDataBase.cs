﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
/// <summary>
/// ステージのID
/// </summary>
public enum StageID
{
    Tutorial,
    STAGE1,
    STAGE2,
    STAGE3,
    CLEAR,
    GAMEOVER,
    None
}
[CreateAssetMenu(fileName = "StageDataBase", menuName = "ScriptableObjects/StageData", order = 1)]
public class StageDataBase : ScriptableObject
{
    public List<StageData> StageDataList = new List<StageData>();
}
[Serializable]
public class StageData
{
    [SerializeField,Tooltip("ステージID")]
    StageID stage = StageID.None;
    [SerializeField, Tooltip("次のステージ")]
    StageID nextStage = StageID.None;
    [SerializeField,Tooltip("１ターン毎に貰えるゴールド")]
    int gold = 0;
    [SerializeField,Tooltip("ステージクリア時の報酬")]
    int clearGold = 0;
    [SerializeField,Tooltip("ターンリミット")]
    int turnLimit = 0;
    [SerializeField,Tooltip("弱い敵のスポーン数")]
    int spawnWeak = 0;
    [SerializeField,Tooltip("普通敵のスポーン数")]
    int spawnNormal = 0;
    [SerializeField,Tooltip("強い敵のスポーン数")]
    int spawnStrong = 0;
    [SerializeField,Tooltip("ボスのスポーン数")]
    int spawnBoss = 0;
    [SerializeField, Tooltip("購入フェイズのBGM")]
    AudioClip buyBGM = null;
    [SerializeField,Tooltip("バトルフェイズのBGM")]
    AudioClip battleBGM = null;
    [SerializeField,Tooltip("クリア画面のBGM")]
    AudioClip clearBGM = null;
    [SerializeField, Tooltip("ゲームオーバー画面のBGM")]
    AudioClip overBGM = null;
    [SerializeField, Tooltip("背景のスプライト画像")]
    Sprite backGround = null;
    /// <summary>
    /// ステージIDを返す
    /// </summary>
    public StageID Stage => stage;
    /// <summary>
    /// 次のステージを返す
    /// </summary>
    public StageID NextStage => nextStage;
    /// <summary>
    /// ターン経過時に貰えるゴールドを返す
    /// </summary>
    public int Gold => gold;
    /// <summary>
    /// ステージクリア時の報酬ゴールドを返す
    /// </summary>
    public int ClearGold => clearGold;
    /// <summary>
    /// ターンリミットを返す
    /// </summary>
    public int TurnLimit => turnLimit;
    /// <summary>
    /// 各敵モンスターのスポーン数を返す
    /// </summary>
    public int[] NumOfSpawnEnemies()
    {
        int[] SpawnEnemies = new int[Enum.GetNames(typeof(EnemyID)).Length];
        SpawnEnemies[(int)EnemyID.Weak] = spawnWeak;
        SpawnEnemies[(int)EnemyID.Normal] = spawnNormal;
        SpawnEnemies[(int)EnemyID.Strong] = spawnStrong;
        SpawnEnemies[(int)EnemyID.Boss] = spawnBoss;
        return SpawnEnemies;
    }
    /// <summary>
    /// 弱い敵のスポーン数を返す
    /// </summary>
    public int SpawnWeak => spawnWeak;
    /// <summary>
    /// 普通敵のスポーン数を返す
    /// </summary>
    public int SpawnNormal => spawnNormal;
    /// <summary>
    /// 強い敵のスポーン数を返す
    /// </summary>
    public int SpawnStrong => spawnStrong;
    /// <summary>
    /// ボスのスポーン数を返す
    /// </summary>
    public int SpawnBoss => spawnBoss;
    /// <summary>
    /// 購入フェイズのBGMを返す
    /// </summary>
    public AudioClip BuyBGM => buyBGM;
    /// <summary>
    /// バトルフェイズのBGMを返す
    /// </summary>
    public AudioClip BattleBGM => battleBGM;
    /// <summary>
    /// 全てのBGMのリストを返す
    /// </summary>
    /// <returns></returns>
    public AudioClip[] GetStageBGMList()
    {
        AudioClip[] BGMList = new AudioClip[(int)BGMs.None];
        BGMList[(int)BGMs.Buy] = buyBGM;
        BGMList[(int)BGMs.Battle] = battleBGM;
        BGMList[(int)BGMs.Clear] = clearBGM;
        BGMList[(int)BGMs.GameOver] = overBGM;
        return BGMList;
    }
    /// <summary>
    /// 背景のスプライト画像を返す
    /// </summary>
    public Sprite BackGround => backGround;
}
