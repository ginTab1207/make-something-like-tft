﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "ItemDataBase", menuName = "ScriptableObjects/ItemData", order = 1)]
public class ItemDataBase : ScriptableObject
{
    public List<ItemData> ItemDataList = new List<ItemData>();
}
[Serializable]
public class ItemData
{
    [SerializeField, Tooltip("アイテム名")]
    ItemID item = ItemID.None;
    [SerializeField, Tooltip("アイテムのプレファブ")]
    GameObject itemPrefab = null;
    public ItemID Item => item;
    public GameObject ItemPrefab => itemPrefab;
}