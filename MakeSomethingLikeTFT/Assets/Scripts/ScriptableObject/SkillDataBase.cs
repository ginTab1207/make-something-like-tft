﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public enum SkillNames
{
    Numenume,
    Healing,
    OverPower,
    Horror,
    Surprise,
    SearchLight,
    OldStone,
    Inspire,
    TetraWave,
    Killer,
    Rose,
    Eternal,
    EvoNumenume,
    EvoHealing,
    EvoOverPower,
    EvoHorror,
    EvoSurprise,
    EvoSearchLight,
    EvoOldStone,
    EvoInspire,
    EvoTetraWave,
    EvoKiller,
    EvoRose,
    EvoEternal,
    None
}
public enum SkillTypes
{
    Attack,
    Support,
    Passive,
    Special,
    None
}
[CreateAssetMenu(fileName = "SkillDataBase", menuName = "ScriptableObjects/SkillData", order = 1)]
public class SkillDataBase : ScriptableObject
{
    public List<SkillData> SkillDataList = new List<SkillData>();
}
[Serializable]
public class SkillData
{
    [SerializeField]
    SkillNames skillName = SkillNames.None;
    [SerializeField]
    SkillTypes skillType = SkillTypes.None;
    [SerializeField]
    float effectPower = 0f;
    [SerializeField]
    float effectTime = 0f;
    [SerializeField]
    float coolDown = 0f;
    [SerializeField,Tooltip("効果対象数,不確定の場合は0")]
    int numOfTargets = 0;
    [SerializeField]
    string skillExplain = null;
    public SkillNames SkillName => skillName;
    public SkillTypes SkillType => skillType;
    public float EffectPower => effectPower;
    public float EffectTime => effectTime;
    public float CoolDown => coolDown;
    public int NumOfTargets => numOfTargets;
    public string SkillExplain => skillExplain;
}
