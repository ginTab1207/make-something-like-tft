﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionCanvas : MonoBehaviour
{
    private Canvas m_Canvas = null;
    void OnEnable()
    {
        m_Canvas = GetComponent<Canvas>();
        m_Canvas.renderMode = RenderMode.ScreenSpaceCamera;
        m_Canvas.worldCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
    }
}
