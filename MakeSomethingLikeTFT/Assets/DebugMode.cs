﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugMode : Singleton<DebugMode>
{
    [SerializeField] bool IsDebug = false;
    [SerializeField] StageID DebugNextStageID = StageID.STAGE1;
    protected override void Awake()
    {
        base.Awake();
        if (IsDebug)
        {
            DontDestroyOnLoad(gameObject);
        }
    }
    public bool GetIsDebug()
    {
        return IsDebug;
    }
    public void Update()
    {
        if (!IsDebug) return;
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            DebugNextStageID = StageID.STAGE1;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            DebugNextStageID = StageID.STAGE2;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            DebugNextStageID = StageID.STAGE3;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            DebugNextStageID = StageID.CLEAR;
        }
        else if (Input.GetKeyDown(KeyCode.O))
        {
            if (DebugNextStageID == StageID.None)
            {
                return;
            }
            BattleManager.Instance.ClearStage(DebugNextStageID);
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            //if (StageManager.Instance.CurrentStageID != StageID.Tutorial) return;
            TutorialManager.Instance.DebugSkipTutorial();
        }
    }
}
