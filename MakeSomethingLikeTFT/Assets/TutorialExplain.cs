﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialExplain : MonoBehaviour
{
    [SerializeField] Text currentText = null;
    public void SetText(string explain)
    {
        currentText.text = explain;
    }
}
