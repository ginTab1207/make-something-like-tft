﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleExplain : MonoBehaviour
{
    private Button m_button = null;
    void OnEnable()
    {
        m_button = GetComponent<Button>();
        m_button.onClick.AddListener(AudioManager.Instance.DestroyExplainPnael);
    }
}
